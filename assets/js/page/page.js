/**
* application logic for udata page
*/

/**
 * Smooth Scroll
 */
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

/**
 * Window scroll handler
 */
var header = $('header'),
    topMenu = header.find('.menu'),
    topMenuToggle = header.find('.menu-link'),
    scrollOffset = 200;

topMenuToggle.on('click', function(e){
    if(topMenu.hasClass('active')){
        topMenu.removeClass('active');
    } else {
        topMenu.addClass('active');
    }
});

$(window).on("scroll", function(e) {
    var activePos = $(window).scrollTop();
    // header
    if (activePos > ($(window).height() - scrollOffset)) {
        header.addClass('snapped');
    } else {
        header.removeClass('snapped');
    }
}).scroll();

$(".sidenav li a").click(function(){
    $(".sidenav li").removeClass('active');
    $(this).parent().addClass('active');
});

$("#sidebar-sticky").stick_in_parent({offset_top: 80});
