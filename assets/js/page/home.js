/**
* application logic for udata homepage
*/

/**
 * Smooth Scroll                            [description]
 */
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

/**
 * Window scroll handler
 */
var header = $('header'),
    topMenu = header.find('.menu'),
    topMenuToggle = header.find('.menu-link'),
    scrollOffset = 200;

topMenuToggle.on('click', function(e){
    if(topMenu.hasClass('active')){
        topMenu.removeClass('active');
    } else {
        topMenu.addClass('active');
    }
});

$(window).on("scroll", function(e) {
    var activePos = $(window).scrollTop() + 450;
    // header
    if (activePos > ($(window).height() - scrollOffset)) {
        header.addClass('snapped');

        // section transition
        $('section').each(function(index){
            if($(this).offset().top <= activePos){
                if ($(window).scrollTop() > 100) {
                    topMenu.find('li').removeClass('active');
                    topMenu.find('li:not(.menu-external)').eq(index).addClass('active');
                } else {
                    topMenu.find('li').removeClass('active-top');
                    topMenu.find('li:not(.menu-external)').eq(index).addClass('active-top');
                }
            }
        });

    } else {
        header.removeClass('snapped');
        if ($(window).scrollTop() > 100) {
            topMenu.find('li.active').removeClass('active');
            topMenu.find('li:first').addClass('active');
        } else {
            topMenu.find('li.active').removeClass('active');
        }
    }

    if ($(window).scrollTop() > 100) {
        $(".menu li").css('color', '#535353');
    } else {
        $(".menu li").css('color', '#FFF');
    }
}).scroll();

/**
 * Slider
 */
var slideCaption = $('.slide-caption'),
    slideTitle = $('.slide-title'),
    slidePagination = $('.slide-pagination'),
    slideImage = $('.slide-image-container'),
    slidePrevious = $('.slide-control-previous'),
    slideNext = $('.slide-control-next'),
    activeIndex = 0,
    slideInterval = 5000;

function activateSlide(index){
    // reset state
    slideTitle.find('span.active').removeClass('active');
    slideImage.find('img.active').removeClass('active');
    slideCaption.find('span.active').removeClass('active');
    slidePagination.find('li.active').removeClass('active');

    // activate all index
    slideTitle.find('span').eq(index).addClass('active');
    slideImage.find('img').eq(index).addClass('active');
    slideCaption.find('span').eq(index).addClass('active');
    slidePagination.find('li').eq(index).addClass('active');

    activeIndex = index;
}

function nextSlide(){
    var totalSlide = slidePagination.find('li').length;
    if ((activeIndex + 1) >= totalSlide) {
        activateSlide(0);
    } else {
        activateSlide(activeIndex + 1);
    }
}

function prevSlide(){
    var totalSlide = slidePagination.find('li').length;
    if((activeIndex - 1) < 0){
        activateSlide(totalSlide - 1);
    } else {
        activateSlide(activeIndex - 1);
    }
}
activateSlide(0);
var slideAutoplay = setInterval(nextSlide, slideInterval);
function resetAutoplay(){
    // reset interval
    clearInterval(slideAutoplay);
    slideAutoplay = setInterval(nextSlide, slideInterval);
}

slidePagination.find('li').on('click', function(e){
    activateSlide($(this).index());
    resetAutoplay();
});
slidePrevious.on('click', function(e){
    prevSlide();
    resetAutoplay();
});
slideNext.on('click', function(e){
    nextSlide();
    resetAutoplay();
});

/**
 * Feature Menu
 */
var featureMenu = $('.feature-menu'),
    featureContent = $('.feature-content');

function openFeatureContent(index){
    featureMenu.find('h3.active').removeClass('active');
    $('.feature-content.active').removeClass('active');

    // activate all index
    featureMenu.find('h3').eq(index).addClass('active');
    featureContent.eq(index).addClass('active');
}
openFeatureContent(0);

featureMenu.find('h3').on('click', function(){
    console.log($(this).index());
    openFeatureContent($(this).index());
});

/**
 * Free Trial
 */
// linkedin loaded
var LISDKLoaded = false;
function onLILoaded(){
    LISDKLoaded = true;
}

// auto fill
var waitForSDK,
    form = $('form[name="free-trial"]');

// auto fill
function autoFillUsingLinkedin(){
    // check if IN SDK already bind to window
    if(!IN || !LISDKLoaded){
        // wait until sdk loaded
        waitForSDK = setTimeout(autoFillUsingLinkedin, 2000);
    }
    if(waitForSDK)
        clearTimeout(waitForSDK);

    if(IN.User.isAuthorized() === false){
        // authorize user
        IN.User.authorize();
    }else{
        IN.User.refresh();
        getLinkedinUserProfile(fillForm);
    }

    IN.Event.on(IN, 'auth', function(e){
        getLinkedinUserProfile(fillForm);
    });

    IN.Event.on(IN, 'logout', function(e){
    });
}

function getLinkedinUserProfile(cb){
    IN.API.Profile('me').fields(
        'first-name',
        'last-name',
        'email-address',
        'location',
        'picture-url',
        'positions'
    ).result(function(result){
        var profile = result.values[0];
        return cb(profile);
    });
}

function fillForm(profile){
    var formValue = {
        'firstName' : profile.firstName || '',
        'lastName' : profile.lastName || '',
        'email' : profile.emailAddress || '',
        'company' : _.get(profile,'positions.values[0].company.name',''),
        'position' :  _.get(profile,'positions.values[0].title',''),
        'address' : _.get(profile,'positions.values[0].company.location.name',''),
    };
    _.each(formValue, function(value, key){
        form.find('input[name="'+key+'"]','textarea[name="'+key+'"]').val(value);
    });
}

form.on('submit', function(e){
    e.preventDefault();
    var fields = {};
    form.find('input','textarea').each(function(index, el){
        el.readOnly = true;
        fields[$(el).attr('name')] = $(el).val();
    });

    // send request to server
    $.ajax({
        type: 'POST',
        url: "home/tryproduct",
        data: fields,
        dataType: 'json'
    })
    .done(function(result){
        if(result.error === false){
            swal({
            imageUrl: 'assets/img/logoudata_dark.png',
            title : "",   
            text: result.message,   
            html: true,
            confirmButtonColor : "#357e7c",
            });
            
            form[0].reset();
            
        } else {
            swal({
            imageUrl: 'assets/img/logoudata_dark.png',
            title : "",   
            text: result.message,   
            html: true,
            confirmButtonColor : "#357e7c"  
            });
            
        }
        form.find('input','textarea').each(function(index, el){
            el.readOnly = false;
        });
    });
});

$('.try-modal-btn').on('click', function (e) {
    $('.modal').modal('hide');
    $('html, body').animate({
        scrollTop: $("#free-trial").offset().top
     }, 1500);
})

$('.modal-header .close').on('click', function (e) {
    $('.modal').modal('hide');
})