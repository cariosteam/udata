/**
* application logic for udata page
*/

/**
 * Smooth Scroll
 */
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

/**
 * Window scroll handler
 */
var header = $('header'),
    topMenu = header.find('.menu'),
    topMenuToggle = header.find('.menu-link'),
    scrollOffset = 200;

topMenuToggle.on('click', function(e){
    if(topMenu.hasClass('active')){
        topMenu.removeClass('active');
    } else {
        topMenu.addClass('active');
    }
});

$(window).on("scroll", function(e) {
    var activePos = $(window).scrollTop();
    // header
    if (activePos > ($(window).height() - scrollOffset)) {
        header.addClass('snapped');
    } else {
        header.removeClass('snapped');
    }
}).scroll();

// linkedin loaded
var LISDKLoaded = false;
function onLILoaded(){
    LISDKLoaded = true;
}

// auto fill
var waitForSDK,
    form = $('#download-insight');

// auto fill
function autoFillUsingLinkedin(){
    // check if IN SDK already bind to window
    if(!IN || !LISDKLoaded){
        // wait until sdk loaded
        waitForSDK = setTimeout(autoFillUsingLinkedin, 2000);
    }
    if(waitForSDK)
        clearTimeout(waitForSDK);

    if(IN.User.isAuthorized() === false){
        // authorize user
        IN.User.authorize();
    }else{
        IN.User.refresh();
        getLinkedinUserProfile(fillForm);
    }

    IN.Event.on(IN, 'auth', function(e){
        getLinkedinUserProfile(fillForm);
    });

    IN.Event.on(IN, 'logout', function(e){
    });
}

function getLinkedinUserProfile(cb){
    IN.API.Profile('me').fields(
        'first-name',
        'last-name',
        'email-address',
        'location',
        'picture-url',
        'positions'
    ).result(function(result){
        var profile = result.values[0];
        return cb(profile);
    });
}

function fillForm(profile){
    var formValue = {
        'firstName' : profile.firstName || '',
        'lastName' : profile.lastName || '',
        'email' : profile.emailAddress || '',
        'company' : _.get(profile,'positions.values[0].company.name',''),
        'position' :  _.get(profile,'positions.values[0].title',''),
        'address' : _.get(profile,'positions.values[0].company.location.name',''),
    };
    _.each(formValue, function(value, key){
        form.find('input[name="'+key+'"]','textarea[name="'+key+'"]').val(value);
    });
}
