$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    // initiate mce
    tinymce.init({
        selector: '.editor',
        menubar: 'edit view',
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
    });

    // initiate color picker
    $('.colorpicker-input').colorpicker();
});