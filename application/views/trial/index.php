<?php if(empty($trials)): ?>
<h2 class="text-muted">trial request is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>name</th>
            <th>job</th>
            <th>phone</th>
            <th>email</th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($trials as $trial): ?>
            <tr>
                <td><?= $trial->firstname.' '.$trial->lastname ?></td>
                <td><?= $trial->position.' at '.$trial->company ?></td>
                <td><?= $trial->phone ?></td>
                <td><?= $trial->email ?></td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>trial/show/<?= $trial->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
