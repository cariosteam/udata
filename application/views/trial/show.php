<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>name</dt>
            <dd><?= $trial->firstname.' '.$trial->lastname ?></dd>
            <dt>job</dt>
            <dd><?= $trial->position.' at '.$trial->company ?></dd>
            <dt>email</dt>
            <dd><?= $trial->email ?></dd>
            <dt>phone</dt>
            <dd><?= $trial->phone ?></dd>
            <dt>requested at</dt>
            <dd><?= $trial->created_at ?></dd>
        </dl>
    </div>
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>address</dt>
            <dd><?= $trial->address ?></dd>
            <dt>notes</dt>
            <dd><?= $trial->notes ?></dd>
        </dl>
    </div>
</div>
