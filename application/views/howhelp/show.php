<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>title</dt>
            <dd><?= $slide->title ?></dd>
            <dt>caption</dt>
            <dd><?= $slide->caption ?></dd>
            <dt>image</dt>
            <dd>
                <img src="<?= upload_url().$slide->image ?>" class="img-thumbnail img-responsive" />
            </dd>
        </dl>
    </div>
</div>
