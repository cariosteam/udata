
<?= form_open_multipart(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $errors ?>
        </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">

        <div class="form-group">
        <?= form_label('Name','name') ?>
        <?= form_input(array(
            'name' => 'name',
            'id' => 'name',
            'maxLength' => '50',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>

        <div class="form-group">
        <?= form_label('Photo','photo') ?>
        <?= form_upload(array(
            'name' => 'photo',
            'id' => 'photo',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
    </div>
    <div class="col-sm-6">

        <div class="form-group">
        <?= form_label('Link','link') ?>
        <?= form_input(array(
            'name' => 'link',
            'id' => 'link',
            'maxLength' => '200',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>

        <div class="form-group">
        <?= form_label('Profile','profile') ?>
        <?= form_textarea(array(
            'name' => 'profile',
            'id' => 'profile',
            'maxLength' => '200',
            'rows' => '4',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Team
    </button>
</div>

<?= form_close() ?>
