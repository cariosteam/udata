<a href="<?= base_url() ?>team/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New team
</a>
<?php if(empty($teams)): ?>
<h2 class="text-muted">team is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th width="30%">name</th>
            <th>link</th>
            <th>profile</th>
            <th width="30%">photo</th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($teams as $team): ?>
            <tr>
                <td><?= $team->name ?></td>
                <td><a href="<?= $team->link ?>"><?= $team->link ?></a></td>
                <td><?= $team->profile ?></td>
                <td>
                    <img src="<?= upload_url().$team->photo ?>" class="img-thumbnail img-responsive" />
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>team/show/<?= $team->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>team/update/<?= $team->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>team/delete/<?= $team->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
