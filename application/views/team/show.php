<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>name</dt>
            <dd><?= $team->name ?></dd>
            <dt>profile</dt>
            <dd><?= $team->profile ?></dd>
            <dt>link</dt>
            <dd><a href="<?= $team->link ?>"><?= $team->link ?></a></dd>
            <dt>photo</dt>
            <dd>
                <img src="<?= upload_url().$team->photo ?>" class="img-thumbnail img-responsive" />
            </dd>
        </dl>
    </div>
</div>
