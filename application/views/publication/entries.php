<h2 class="text-center text-success">Related Insights</h2>

<!-- publication loops -->
<div class="row publication-container">
<?php foreach ($publications as $publication): ?>
<div class="col-sm-4 col-xs-12">
    <a href="<?= base_url() ?>insight/<?= $publication->id ?>" class="row middle-xs publication-entry">
        <div class="col-xs-4">
            <img src="<?= upload_url().$publication->cover ?>" class="illustration" />
        </div>
        <div class="col-xs-8">
            <h3><?= $publication->title ?></h3>
            <h5>by <?= $publication->author ?></h5>

        </div>
    </a>
</div>
<?php endforeach; ?>
</div>

<!-- pagination -->
<div class="row middle-xs center-xs">
<div class="publication-pagination">
    <?= $all_pages ?>
</div>
</div>
