<div class="row center-xs">
    <h2 class="text-primary col-xs-12 col-sm-8 text-center">Download <?= $publication->title ?></h2>
    <!-- filling form -->
    <form target="_self" method="post" class="row col-sm-8" name="download-insight" id="download-insight">
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <button type="button" onclick="autoFillUsingLinkedin()" class="btn btn-linkedin btn-block">
                    <span class="icon icon-social-linkedin"></span>
                    autofill using linkedIn
                </button>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12"></div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <label>name</label>
                <div class="row between-xs">
                    <input type="text"
                    name="firstName" placeholder="first name" class="form-control col-xs" />
                    <input type="text" name="lastName" placeholder="last name" class="form-control col-xs" required=""/>
                </div>
            </div>
            <div class="form-group">
                <label>company</label>
                <input type="text" name="company" placeholder="your current company" class="form-control" required=""/>
            </div>
            <div class="form-group">
                <label>position</label>
                <input type="text" name="position" placeholder="position on your company" class="form-control" required=""/>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <label>email</label>
                <input type="text" name="email" placeholder="your work email" class="form-control" required=""/>
            </div>
            <div class="form-group">
                <label>phone number</label>
                <input type="text" name="phone" placeholder="+(62) xxx" class="form-control" required=""/>
            </div>
            <div class="form-group">
                <label>address</label>
                <input type="text" name="address" placeholder="jl. jalan 12. kota. bandung" class="form-control" required=""/>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">
                    <span class="icon icon-ios-cloud-download"></span>
                    download publication
                </button>
            </div>
        </div>
    </form>
</div>
</div>
