<a href="<?= base_url() ?>publication/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Publication
</a>
<?php if(empty($publications)): ?>
<h2 class="text-muted">publication is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>title</th>
                <th>author</th>
            <th>content</th>
            <th>file</th>
            <th>downloaded</th>
            <th width="20%">cover</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($publications as $publication): ?>
            <tr>
                <td><?= $publication->title ?></td>
                <td><?= $publication->author ?></td>
                <td><?= substr($publication->content, 0 ,300) ?></td>
                <td><?= $publication->file ?></td>
                <td>
                    <?php
                        if(!empty($publication->readers)){
                            echo $publication->readers[0]->counted_rows;
                        } else {
                            echo 0;
                        }
                    ?>
                </td>
                <td>
                    <img src="<?= upload_url().$publication->cover ?>" class="img-thumbnail img-responsive" />
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>publication/show/<?= $publication->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>publication/update/<?= $publication->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>publication/delete/<?= $publication->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
