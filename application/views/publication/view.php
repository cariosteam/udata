<div class="row center-xs">
    <div class="col-xs-12 col-sm-8 publication">
        <div class="publication-title">
            <h2><?= $publication->title ?></h2>
            <h5>by <?= $publication->author ?></h5>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-12 publication-meta">
                <img src="<?= upload_url().$publication->cover ?>" class="illustration" />
                <a href="<?= base_url() ?>download-insight/<?= $publication->id ?>" class="btn btn-block btn-primary download-btn">
                    <span class="icon icon-ios-cloud-download"></span>
                    download publication
                </a>
            </div>
            <div class="col-sm-8 col-xs-12 publication-content">
                <?= $publication->content ?>
            </div>
        </div>
    </div>
</div>
