<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>title</dt>
            <dd><?= $publication->title ?></dd>
            <dt>author</dt>
            <dd><?= $publication->author ?></dd>
            <dt>file</dt>
            <dd><?= $publication->file ?></dd>
            <dt>downloaded</dt>
            <dd>
                <?php
                    if(!empty($publication->readers)){
                        echo $publication->readers[0]->counted_rows;
                    } else {
                        echo 0;
                    }
                ?>
            </dd>
            <dt>cover</dt>
            <dd>
                <img src="<?= upload_url().$publication->cover ?>" class="img-thumbnail img-responsive" />
            </dd>
        </dl>
    </div>
    <div class="col-sm-6">
        <h1><?= $publication->title ?></h1>
        <?= $publication->content ?>
    </div>
</div>
