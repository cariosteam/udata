<a href="<?= base_url() ?>feature/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Feature
</a>
<?php if(empty($features)): ?>
<h2 class="text-muted">feature is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>name</th>
            <th>title</th>
            <th>content</th>
            <th width="30%">illustration</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($features as $feature): ?>
            <tr>
                <td><?= $feature->name ?></td>
                <td><?= $feature->title ?></td>
                <td><?= substr($feature->content, 0, 200) ?></td>
                <td>
                    <img src="<?= upload_url().$feature->illustration ?>" class="img-thumbnail img-responsive" />
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>feature/show/<?= $feature->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>feature/update/<?= $feature->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>feature/delete/<?= $feature->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
