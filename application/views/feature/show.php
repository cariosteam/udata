<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>name</dt>
            <dd><?= $feature->name ?></dd>
            <dt>title</dt>
            <dd><?= $feature->title ?></dd>
            <dt>illustration</dt>
            <dd>
                <img src="<?= upload_url().$feature->illustration ?>" class="img-thumbnail img-responsive" />
            </dd>
        </dl>
    </div>
    <div class="col-sm-6">
        <h1><?= $feature->title ?></h1>
        <?= $feature->content ?>
    </div>
</div>
