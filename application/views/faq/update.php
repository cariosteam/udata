
<?= form_open_multipart(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $errors ?>
            </div>
        <?php endif ?>
    </div>
</div>
<div class="row">

    <div class="col-sm-12">
        <?php

        $option = [];
        foreach ($kategori as $cat) {
            $option[$cat->id_category] = $cat->category;
        }
        ?>
        <div class="form-group">
            <?= form_label('Category','category') ?>
            <?= form_dropdown(array(
                'name' => 'category',
                'id' => 'category',
                'maxLength' => '255',
                'required' => 'true',
                'class' => 'form-control'
            ),$option,$faq->id_category) ?>
        </div>
    </div>

    <div class="col-sm-6">

        <div class="form-group">
            <?= form_label('Title','title') ?>
            <?= form_input(array(
                'name' => 'title',
                'id' => 'title',
                'maxLength' => '50',
                'required' => 'true',
                'class' => 'form-control'
            ), $faq->title) ?>
        </div>
        <div class="form-group">
            <?= form_label('Question','question') ?>
            <?= form_input(array(
                'name' => 'question',
                'id' => 'question',
                'maxLength' => '50',
                'required' => 'true',
                'class' => 'form-control'
            ), $faq->question) ?>
        </div>
    </div>


    <div class="col-sm-12">
        <?= form_label('Answer','answer') ?>
        <?= form_textarea(array(
            'name' => 'answer',
            'id' => 'answer',
            'rows' => '8',
            'required' => 'true',
            'class' => 'form-control editor'
        ), $faq->answer) ?>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save <?= $faq->title ?>
    </button>
</div>

<?= form_close() ?>
