
<a href="<?= base_url() ?>faq/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Feature
</a>
<?php if(empty($faq)): ?>
<h2 class="text-muted">faq is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>title</th>
            <th>question</th>
            <th>answer</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($faq as $feature): ?>
            <tr>
                <td><?= $feature->title ?></td>
                <td><?= $feature->question ?></td>
                <td><?= substr($feature->answer, 0, 200) ?></td>

                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>faq/show/<?= $feature->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>faq/update/<?= $feature->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>faq/delete/<?= $feature->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
