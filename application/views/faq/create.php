
<?= form_open_multipart(current_url(), array('novalidate'=>'true')) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $errors ?>
            </div>
        <?php endif ?>
    </div>
</div>
<div class="row">

    <div class="col-sm-12">
        <?php

        $option = [];
        foreach ($kategori as $cat) {
            $option[$cat->id_category] = $cat->category;
        }

        ?>
        <div class="form-group">
            <?= form_label('Category','category') ?>
            <?= form_dropdown(array(
                'name' => 'category',
                'id' => 'category',
                'maxLength' => '255',
                'required' => 'true',
                'class' => 'form-control'
            ),$option) ?>
        </div>
    </div>

    <div class="col-sm-6">

        <div class="form-group">
            <?= form_label('Title','title') ?>
            <?= form_input(array(
                'name' => 'title',
                'id' => 'title',
                'maxLength' => '50',
                'required' => 'true',
                'class' => 'form-control'
            )) ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <?= form_label('Question','question') ?>
            <?= form_input(array(
                'name' => 'question',
                'id' => 'question',
                'maxLength' => '255',
                'required' => 'true',
                'class' => 'form-control'
            )) ?>
        </div>
    </div>



    <div class="col-sm-12">
        <?= form_label('Jawaban','jawaban') ?>
        <?= form_textarea(array(
            'name' => 'jawaban',
            'id' => 'jawaban',
            'rows' => '8',
            'required' => 'true',
            'class' => 'form-control editor'
        )) ?>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Faq
    </button>
</div>

<?= form_close() ?>
