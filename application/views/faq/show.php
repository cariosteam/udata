<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>name</dt>
            <dd><?= $faq->title ?></dd>
            <dt>title</dt>
            <dd><?= $faq->question ?></dd>
        </dl>
    </div>
    <div class="col-sm-6">
        <h1><?= $faq->question ?></h1>
        <?= $faq->answer ?>
    </div>
</div>
