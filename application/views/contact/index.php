
<?= form_open(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $errors ?>
            </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?= form_label('Address','address') ?>
            <?= form_textarea(array(
                'name' => 'address',
                'id' => 'address',
                'rows' => '5',
                'class' => 'form-control'
            ), $contact->address) ?>
        </div>

        <div class="form-group">
            <?= form_label('City','city') ?>
            <?= form_input(array(
                'name' => 'city',
                'id' => 'city',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->city) ?>
        </div>

        <div class="form-group">
            <?= form_label('State / Province','state') ?>
            <?= form_input(array(
                'name' => 'state',
                'id' => 'state',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->state) ?>
        </div>

        <div class="form-group">
            <?= form_label('Zipcode','zipcode') ?>
            <?= form_input(array(
                'name' => 'zipcode',
                'id' => 'zipcode',
                'maxLength' => '10',
                'class' => 'form-control'
            ), $contact->zipcode) ?>
        </div>

        <div class="form-group">
            <?= form_label('Phone','phone') ?>
            <?= form_input(array(
                'name' => 'phone',
                'id' => 'phone',
                'maxLength' => '15',
                'class' => 'form-control'
            ), $contact->phone) ?>
        </div>
    </div>
    <div class="col-sm-6">

        <div class="form-group">
            <?= form_label('Fax','fax') ?>
            <?= form_input(array(
                'name' => 'fax',
                'id' => 'fax',
                'maxLength' => '15',
                'class' => 'form-control'
            ), $contact->fax) ?>
        </div>

        <div class="form-group">
            <?= form_label('Twitter','twitter') ?>
            <?= form_input(array(
                'name' => 'twitter',
                'id' => 'twitter',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->twitter) ?>
        </div>

        <div class="form-group">
            <?= form_label('Facebook','facebook') ?>
            <?= form_input(array(
                'name' => 'facebook',
                'id' => 'facebook',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->facebook) ?>
        </div>

        <div class="form-group">
            <?= form_label('LinkedIn','linkedin') ?>
            <?= form_input(array(
                'name' => 'linkedin',
                'id' => 'linkedin',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->linkedin) ?>
        </div>

        <div class="form-group">
            <?= form_label('Latitude','lat') ?>
            <?= form_input(array(
                'name' => 'lat',
                'id' => 'lat',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->lat) ?>
        </div>

        <div class="form-group">
            <?= form_label('Longitude','long') ?>
            <?= form_input(array(
                'name' => 'long',
                'id' => 'long',
                'maxLength' => '50',
                'class' => 'form-control'
            ), $contact->long) ?>
        </div>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Contact
    </button>
</div>

<?= form_close() ?>
