<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Udata - Dashboard Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">

    <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="<?= asset_url() ?>css/dashboard/login.css">
</head>

<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="container">

        <form class="form-signin" target="_self" method="post">
            <h2 class="form-signin-heading">Dashboard Landing Page</h2>
            <label class="sr-only">username</label>
            <input type="text" name="name" class="form-control" placeholder="username" required autofocus>
            <label class="sr-only">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
                Sign in
            </button>

            <br/>
            <?php if(validation_errors()): ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?php if(isset($unauthorize) && $unauthorize === TRUE): ?>
                <div class="alert alert-danger" role="alert">
                    username atau password tidak cocok
                </div>
            <?php endif; ?>
        </form>

    </div>
</body>

<script src="<?= asset_url() ?>js/lib/jquery-2.2.0.min.js"></script>
<script src="<?= asset_url() ?>js/lib/bootstrap.min.js"></script>
<!-- app -->
<script src="<?= asset_url() ?>js/dashboard/login.js" type="text/javascript"></script>
</html>
