<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Udata - Social Media Analytics</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/app.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/page/page.css">
    </head>
    <body class="page">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php $this->load->view('layouts/header');  ?>

        <section class="bg-light">
            <div class="container">
                <div id="sidebar-sticky" class="col-md-4">
                    <ul class="nav nav-list sidenav">
                        <?php
                        foreach ($category as $cat) {
                        ?>
                        <li><a href="<?php echo '#'.$cat->alias;?>"><i class="icon-chevron-right"></i> <?php echo $cat->category;?></a></li>
                        <?php  } ?>
                    </ul>
                </div>
                <div class="col-md-8">
                    <?php foreach ($category as $cat) { ?>
                    <div id="<?php echo $cat->alias;?>" class="panel-group faq-content">
                        <p class="topic-title"><?php echo $cat->category ;?></p>

                        <?php
                        $questionpercategory = $this->Faq_model->listPerCategory($cat->id_category);
                            foreach ($questionpercategory as $qpc) {
                            ?>
                        <div class="panel panel-default">
                            <div id="<?php echo $cat->alias;?>" class="panel-heading">
                                <h4 class="panel-title" data-toggle="collapse" data-target="<?php echo '#'.$qpc->alias;?>" >
                                    <?php echo $qpc->title ?>
                                </h4>
                            </div>
                            <div id="<?php echo $qpc->alias;?>" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li class="list-group-item">Question:</li>
                                    <li class="list-group-item question"><?php echo $qpc->question;?></li>
                                    <li class="list-group-item">Answer:</li>
                                    <li class="list-group-item answer"><?php echo $qpc->answer;?></li>
                                </ul>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php }?>
                </div>
            </div>
        </section>

        <?php $this->load->view('layouts/footer');  ?>
    </body>

    <!-- dependency -->
    <script src="<?= asset_url() ?>js/lib/jquery-2.2.0.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/bootstrap.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/jquery.sticky-kit.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/lodash.min.js"></script>
    <!-- app -->
    <script src="<?= asset_url() ?>js/page/page.js" type="text/javascript"></script>
</html>
