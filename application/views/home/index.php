<!-- how we help you -->
<section id="how-we-help-you" class="bg-gradient">
    <!-- image -->
    <div class="slide-image-container">
        <?php foreach ($slides as $key => $slide): ?>
        <img class="<?= $key === 0?'active':''?>" src="<?= upload_url().$slide->image ?>" />
        <?php endforeach; ?>
    </div>
    <!-- caption -->
    <div class="slide-caption-container">
        <div class="container">
            <div class="row center-xs middle-xs text-center">
                <div class="col-sm-8 col-xs-12 col between-x center-xs full-height">
                    <div class="row-xs-4"></div>
                    <div class="row-xs-6">
                        <h1 class="slide-title">
                            <?php foreach ($slides as $key => $slide): ?>
                            <span class="<?= $key === 0?'active':''?>">
                            <?= $slide->title ?>
                            </span>
                            <?php endforeach; ?>
                        </h1>
                        <div class="row center-xs middle-xs">
                            <!-- control -->
                            <div class="col-xs-2 slide-control-previous">
                                <span class="icon icon-chevron-left"></span>
                            </div>
                            <!-- caption text -->
                            <h3 class="col-sm-8 col-xs-8 slide-caption">
                                <?php foreach ($slides as $key => $slide): ?>
                                <span class="<?= $key === 0?'active':''?>">
                                <?= $slide->caption ?>
                                </span>
                                <?php endforeach; ?>
                            </h3>
                            <!-- control -->
                            <div class="col-xs-2 slide-control-next">
                                <span class="icon icon-chevron-right"></span>
                            </div>
                        </div>
                        <!-- pagination -->
                        <ul class="slide-pagination">
                            <?php foreach ($slides as $key => $slide): ?>
                            <li class="<?= $key === 0?'active':''?>"></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- scroll -->
                    <div class="row-xs-2 scroll-more">
                        <h1 class="icon icon-ios-arrow-down up-and-down"></h1>
                        <h6>scroll to get more..</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- why us -->
<section id="why-us" class="bg-clear">
    <div class="container text-center">
        <div class="row center-xs middle-xs center-xs full-height">
            <?php foreach ($whys as $why): ?>
            <div id="why-us-<?= $why->id ?>" class="col-sm col-xs-12 why-us-item" data-toggle="modal" data-target="#why-us-modal-<?= $why->id ?>">
                <h2 class="title"><?= $why->title ?></h2>
                <img class="illustration" src="<?= upload_url().$why->image ?>" width="100%" />
                <h4 class="description">
                    <?= $why->caption ?>
                </h4>
            </div>
            <?php endforeach; ?>
        </div>

        <?php foreach ($whys as $why): ?>
        <div class="modal fade why-us-modal" id="why-us-modal-<?= $why->id ?>" tabindex="-1" role="dialog" aria-labelledby="why-us-modal-<?= $why->id ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center"><?= $why->title ?></h4>
                    </div>
                    <div class="modal-body">
                        <img src="<?= upload_url().$why->image ?>" />
                        <p><?= $why->content ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default try-modal-btn" data-dismiss="modal">TRY FOR FREE</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

</section>
<!-- features -->
<section id="features" class="bg-success">
    <div class="container">
        <div class="row middle-xs center-xs full-height">
            <div class="col-sm-12">
                <div class="row center-xs">
                    <div class="col-sm-3 col-xs-12 feature-menu">
                        <?php foreach ($features as $feature): ?>
                        <h3><?= $feature->name ?> <span class="indicator"></span></h3>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-9 feature-content-container">
                        <?php foreach ($features as $key => $feature): ?>
                        <div class="row center-xs around-xs top-xs feature-content <?= $key === 0?'active':''?>">
                            <div class="col-sm-5 col-xs-12 feature-illustration ">
                                <img src="<?= asset_url() ?>img/feature-screen.png" class="screen" width="100%" />
                                <img class="illustration" src="<?= upload_url().$feature->illustration ?>" width="100%" />
                            </div>
                            <div class="col-sm-5 col-xs-12">
                                <h2><?= $feature->title ?></h2>
                                <?= $feature->content ?>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our team -->
<section id="our-team" class="bg-clear">
    <div class="container">
        <div class="row middle-xs full-height">
            <h3 class="our-team-heading">OUR AMAZING TEAM</h3>
            <div class="col-xs-12 row top-xs center-xs container">
                <?php foreach ($teams as $team): ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="<?= $team->link ?>">
                        <div class="team-profile">
                            <img src="<?= upload_url().$team->photo ?>" />
                            <div class="profile-container">
                                <h3><?= $team->name ?></h3>
                                <p><?= $team->profile ?></p>
                            </div>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- pricing -->
<section id="pricing" class="bg-success">
    <div class="container">
        <div class="row">
            <?php foreach ($pricing_plans as $plan): ?>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="plan first">
                    <div class="head" style="background-color:<?= $plan->color ?>;">
                        <h2><?= ucfirst($plan->name) ?></h2>
                    </div>
                    <ul class="item-list">
                        <!--<li><strong><?php print_r($plan->spesifications); ?></strong></li>-->
                        <li><strong><?php print_r($plan->spesifications->features->trackers_alert); ?></strong> Tracker Alert</li>
                        <li><strong><?php print_r($plan->spesifications->features->customized_report); ?></strong> Customized Report</li>
                        <li><strong><?php print_r($plan->spesifications->features->customer_support); ?></strong> Customer Support</li>
                        <li><strong><?php print_r($plan->spesifications->limitation->keywords); ?></strong> Keywords</li>
                        <li><strong><?php print_r($plan->spesifications->limitation->trackers); ?></strong> Trackers</li>
                        <li><strong><?php print_r($plan->spesifications->limitation->reports_created); ?></strong> Reports Created</li>
                        <li><strong><?php print_r($plan->spesifications->limitation->team_members); ?></strong> Team Members</li>
                    </ul>
                    <div class="price">
                        <h3><?php print_r($plan->spesifications->Investment->harga); ?></h3>
                        <h4>Monthly Subscription</h4>
                    </div>
                    <div class="price">
                        <h3><?php print_r($plan->spesifications->Investment->Annual_subscription); ?></h3>
                        <h4>Annual Subscription (Free 1 Month)</h4>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

            <!--
            <div class="col-sm-4 col-md-4 ">
                <div class="plan recommended">
                    <div class="head">
                        <h2>Value</h2>
                    </div>
                    <ul class="item-list">
                        <li><strong>50GB</strong> Storage</li>
                        <li><strong>25</strong> Email Addresses</li>
                        <li><strong>15</strong> Domains</li>
                        <li><strong>Endless</strong> Support</li>
                    </ul>
                    <div class="price">
                        <h3><span class="symbol">Rp.</span>29M</h3>
                        <h4>per month</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4  ">
                <div class="plan last">
                    <div class="head">
                        <h2>Pro</h2>
                    </div>
                    <ul class="item-list">
                        <li><strong>100GB </strong>Storage</li>
                        <li><strong>50 </strong>Email Addresses</li>
                        <li><strong>25</strong> Domains</li>
                        <li><strong>Endless</strong> Support</li>
                    </ul>
                    <div class="price">
                        <h3><span class="symbol">Rp.</span>49M</h3>
                        <h4>per month</h4>
                    </div>
                </div>
            </div>
        </div>
        -->
    </div>
</section>
<!-- free trial -->
<section id="free-trial" class="bg-clear">
    <div class="container">
        <div class="row center-xs middle-xs full-height">
            <div class="col-sm-8 col-xs-12">
                <!-- filling form -->
                <form class="row" name="free-trial">
                <?php echo validation_errors(); ?>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Name<font color="red">*</font></label>
                            <div class="row between-xs name-input">
                                <input type="text" name="firstName" placeholder="First name" class="form-control col-xs"  required=""/>
                                <input type="text" name="lastName" placeholder="Last name" class="form-control col-xs" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Company<font color="red">*</font></label>
                            <input type="text" name="company" placeholder="Your current company" class="form-control" required=""/>
                        </div>
                        <div class="form-group">
                            <label>Position<font color="red">*</font></label>
                            <input type="text" name="position" placeholder="Position on your company" class="form-control" required=""/>
                        </div>
                        <div class="form-group">
                            <label>Address<font color="red">*</font></label>
                            <input type="text" name="address" placeholder="JL. Jalan 12. Kota. Bandung" class="form-control" required=""/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Email<font color="red">*</font></label>
                            <input type="email" name="email" placeholder="We will use this for registration" class="form-control" required="" value="<?php echo set_value('email'); ?>"/>
                            <?php echo form_error('email'); ?>
                        </div>
                        <div class="form-group">
                            <label>Phone Number<font color="red">*</font></label>
                            <input type="text" name="phone" placeholder="+(62) xxx" class="form-control" required=""/>
                        </div>
                        <div class="form-group">
                            <label>Notes</label>
                            <textarea type="text" name="notes" placeholder="Tell us what you want and what package you interested with" rows="5" class="form-control" required="" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <button type="button" onclick="autoFillUsingLinkedin()" class="btn btn-linkedin btn-block">
                            <span class="icon icon-social-linkedin"></span>
                            AUTOFILL USING LINKEDIN
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                            <span class="icon icon-paper-airplane"></span>
                            SUBMIT FOR FREE TRIAL
                            </button>
                        </div><br>
                        <?php if(validation_errors()): ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors('email'); ?>
                </div>
            <?php endif; ?>
                    </div><br>

                </form>
            </div>
            <div class="col-sm-4 col-xs-11 first-sm">
                <!-- maps -->
                <div class="maps">
                    <div id="gmap_canvas"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyD2zzXQOxbdqKfCuJ1zmojcA7CtCH4qjBo"></script>
    <script>
       // initiate maps configs
       google.maps.event.addDomListener(window, 'load', function(){
           var myOptions = {
               zoom:15,
               center:new google.maps.LatLng(<?= $contact->lat ?>, <?= $contact->long ?>),
               mapTypeId: google.maps.MapTypeId.ROADMAP
           };
           map = new google.maps.Map(
               document.getElementById("gmap_canvas"), myOptions);
           marker = new google.maps.Marker({
               map: map,
               position: new google.maps.LatLng(<?= $contact->lat ?>, <?= $contact->long ?>)
           });
           infowindow = new google.maps.InfoWindow({
               content:"<b>Divisi Digital Telkom Indonesia</b><br/><?= $contact->address ?><br/><?= $contact->zipcode ?> <?= $contact->city ?>"
           });
           google.maps.event.addListener(marker, "click",
               function(){
                   infowindow.open(map,marker);
           });
           infowindow.open(map,marker);
       });
    </script>
    <script type="text/javascript" src="//platform.linkedin.com/in.js">
        api_key:   75ipi5mcus2kpb
        onLoad:    onLILoaded
        scope: r_basicprofile r_emailaddress
        authorize: false
        lang:      in_ID
    </script>