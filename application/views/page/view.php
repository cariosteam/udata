<div class="row page-title center-xs">
    <div class="col-sm-6">
        <h2><?= ucfirst($page->title) ?></h2>
        <?php if(!empty($page->subtitle)): ?>
            <h4><?= $page->subtitle ?></h4>
        <?php endif; ?>
    </div>
</div>
<?= $page->content ?>
