
<?= form_open(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $errors ?>
            </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">

        <div class="form-group">
            <?= form_label('Title','title') ?>
            <?= form_input(array(
                'name' => 'title',
                'id' => 'title',
                'maxLength' => '100',
                'required' => 'true',
                'class' => 'form-control'
            ), $page->title) ?>
        </div>

        <div class="form-group">
            <?= form_label('Subtitle','subtitle') ?>
            <?= form_input(array(
                'name' => 'subtitle',
                'id' => 'subtitle',
                'maxLength' => '200',
                'required' => 'true',
                'class' => 'form-control'
            ), $page->subtitle) ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <?= form_label('Slug','slug') ?>
            <?= form_input(array(
                'name' => 'slug',
                'id' => 'slug',
                'maxLength' => '100',
                'class' => 'form-control'
            ), $page->slug) ?>
        </div>
    </div>

    <div class="col-sm-12">
        <?= form_label('Content','content') ?>
        <?= form_textarea(array(
            'name' => 'content',
            'id' => 'content',
            'rows' => '8',
            'required' => 'true',
            'class' => 'form-control editor'
        ), $page->content) ?>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save <?= $page->title ?>
    </button>
</div>

<?= form_close() ?>
