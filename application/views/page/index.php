<a href="<?= base_url() ?>page/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Page
</a>
<?php if(empty($pages)): ?>
<h2 class="text-muted">page is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>title</th>
            <th>subtitle</th>
            <th>slug</th>
            <th width="20%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($pages as $page): ?>
            <tr>
                <td><?= $page->title ?></td>
                <td><?= $page->subtitle ?></td>
                <td><?= $page->slug ?></td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>page/<?= $page->slug ?>" class="btn btn-default" data-toggle="tooltip" title="preview">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                        <a href="<?= base_url() ?>page/show/<?= $page->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>page/update/<?= $page->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>page/delete/<?= $page->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
