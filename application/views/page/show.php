<div class="row">
    <div class="col-sm-6">
        <dl class="dl-horizontal">
            <dt>title</dt>
            <dd><?= $page->title ?></dd>
            <dt>slug</dt>
            <dd><?= $page->slug ?></dd>
            <dt>subtitle</dt>
            <dd><?= $page->subtitle ?></dd>
        </dl>
    </div>
    <div class="col-sm-6">
        <?= $page->content ?>
    </div>
</div>
