
<?= form_open_multipart(current_url(), array('novalidate'=>'true')) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $errors ?>
            </div>
        <?php endif ?>
    </div>
</div>
<div class="row">

    <div class="col-sm-6">

        <div class="form-group">
            <?= form_label('Category','category') ?>
            <?= form_input(array(
                'name' => 'category',
                'id' => 'category',
                'maxLength' => '200',
                'required' => 'true',
                'class' => 'form-control'
            )) ?>
        </div>
    </div>

    <div class="col-sm-12">
        <?= form_label('Description','description') ?>
        <?= form_textarea(array(
            'name' => 'description',
            'id' => 'description',
            'rows' => '8',
            'required' => 'true',
            'class' => 'form-control editor'
        )) ?>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Faq Category
    </button>
</div>

<?= form_close() ?>
