<a href="<?= base_url() ?>whyus/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Slide
</a>
<?php if(empty($slides)): ?>
<h2 class="text-muted">slide is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th width="20%">Title</th>
            <th>Caption</th>
            <th>Content</th>
            <th width="20%">Image</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($slides as $slide): ?>
            <tr>
                <td><?= $slide->title ?></td>
                <td><?= $slide->caption ?></td>
                <td><?= $slide->content ?></td>
                <td>
                    <img src="<?= upload_url().$slide->image ?>" class="img-thumbnail img-responsive" />
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>whyus/show/<?= $slide->id ?>" class="btn btn-default" data-toggle="tooltip" title="detail">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                        <a href="<?= base_url() ?>whyus/update/<?= $slide->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>whyus/delete/<?= $slide->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
