
<?= form_open_multipart(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $errors ?>
        </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">

        <div class="form-group">
        <?= form_label('Title','title') ?>
        <?= form_input(array(
            'name' => 'title',
            'id' => 'title',
            'maxLength' => '50',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>

        <div class="form-group">
        <?= form_label('Image','image') ?>
        <?= form_upload(array(
            'name' => 'image',
            'id' => 'image',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
        <div class="form-group">
        <?= form_label('Content','content') ?>
        <?= form_textarea(array(
            'name' => 'content',
            'id' => 'content',
            'maxLength' => '200',
            'rows' => '4',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
    </div>
    <div class="col-sm-6">

        <div class="form-group">
        <?= form_label('Caption','caption') ?>
        <?= form_textarea(array(
            'name' => 'caption',
            'id' => 'caption',
            'maxLength' => '200',
            'rows' => '4',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Slide
    </button>
</div>

<?= form_close() ?>
