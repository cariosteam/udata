<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Udata - Social Media Analytics</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/app.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/page/page.css">
    </head>
    <body class="page">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php require_once('header.php');  ?>

        <section class="bg-light">
            <div class="container">
                <div class="row center-xs">
                <div class="col-sm-8 col-xs-12">
                    <?= $yield ?>
                </div>                    
                </div>
            </div>
            </div>
        </section>
        <?php require_once('footer.php');  ?>
    </body>

    <!-- dependency -->
    <script src="<?= asset_url() ?>js/lib/jquery-2.2.0.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/bootstrap.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/jquery.sticky-kit.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/lodash.min.js"></script>
    <!-- app -->
    <script src="<?= asset_url() ?>js/page/page.js" type="text/javascript"></script>
</html>
