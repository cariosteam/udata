<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>UData - Social Media Analytics</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="<?= base_url() ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?= base_url() ?>/favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/app.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/page/home.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="<?= asset_url() ?>css/sweetalert.css">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php require_once('header.php');  ?>

        <?= $yield ?>

        <?php require_once('footer.php');  ?>

    </body>

    <!-- dependency -->

    <script src="<?= asset_url() ?>js/lib/jquery-2.2.0.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/bootstrap.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/lodash.min.js"></script>
    <!-- app -->
    <script src="<?= asset_url() ?>js/page/home.js" type="text/javascript"></script>
    <script src="<?= asset_url() ?>js/page/sweetalert.min.js" type="text/javascript"></script>
</html>
