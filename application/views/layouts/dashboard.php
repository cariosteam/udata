<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Udata - <?= ucfirst($title) ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="<?= base_url() ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/favicon.ico">

    <link rel="stylesheet" href="<?= asset_url() ?>css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="<?= asset_url() ?>css/lib/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="<?= asset_url() ?>css/lib/colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="<?= asset_url() ?>css/dashboard/app.css">
</head>

<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <nav class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm navmenu-inverse">
        <a class="navmenu-brand visible-md visible-lg" href="#">Landing Page Content Editor</a>
        <ul class="nav navmenu-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    How we help you
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>howhelp">List</a></li>
                    <li><a href="<?= base_url() ?>howhelp/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Why Us
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>whyus">List</a></li>
                    <li><a href="<?= base_url() ?>whyus/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Team
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>team">List</a></li>
                    <li><a href="<?= base_url() ?>team/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Feature
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>feature">List</a></li>
                    <li><a href="<?= base_url() ?>feature/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Pricing
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li class="dropdown-header">Plan</li>
                    <li><a href="<?= base_url() ?>pricing">Plan List</a></li>
                    <li><a href="<?= base_url() ?>pricing/create">Create Plan</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Specification</li>
                    <li><a href="<?= base_url() ?>pricing/specs">Specification List</a></li>
                    <li><a href="<?= base_url() ?>pricing/specgroups">Specification Group List</a></li>
                </ul>
            </li>
            <li><a href="<?= base_url() ?>trial">Trial Submission</a></li>
            <li><a href="<?= base_url() ?>contact">Contact and Address</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Page
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>page">List</a></li>
                    <li><a href="<?= base_url() ?>page/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    FAQ
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>faq/faq_list">List</a></li>
                    <li><a href="<?= base_url() ?>faq/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    FAQ Category
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>faq/category">List</a></li>
                    <li><a href="<?= base_url() ?>faq/category/create">Create</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Publication
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu navmenu-nav">
                    <li><a href="<?= base_url() ?>publication">List</a></li>
                    <li><a href="<?= base_url() ?>publication/create">Create</a></li>
                </ul>
            </li>
            <li><a href="<?= base_url() ?>admin">Administrator</a></li>
            <li><a href="<?= base_url() ?>dashboard/logout">Logout<span class="glyphicon glyphicon-off pull-right"></span></a></li>
        </ul>
    </nav>

    <div class="navbar navbar-default navbar-fixed-top hidden-md hidden-lg">
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Landing Page Content Editor</a>
    </div>

    <div class="container">
        <div class="page-header">
            <h1><?= ucfirst($title) ?></h1>
        </div>
        <?= $yield ?>
    </div><!-- /.container -->

    </body>

    <script src="<?= asset_url() ?>js/lib/jquery-2.2.0.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/bootstrap.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/jasny-bootstrap.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/tinymce/tinymce.min.js"></script>
    <script src="<?= asset_url() ?>js/lib/bootstrap-colorpicker.min.js"></script>
    <!-- app -->
    <script src="<?= asset_url() ?>js/dashboard/app.js" type="text/javascript"></script>
</html>
