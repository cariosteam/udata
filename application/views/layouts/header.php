<!-- header -->
<header>
    <div class="header-container">
        <div class="row">
            <div class="col-sm-2 col-xs-5 brand" onclick="location.href='<?= base_url() ?>'">
                <span class="brand-image"></span>
                <span class="brand-caption">
                    Social Media<br/> Analytics
                </span>
            </div>
            <div class="col-sm-10 col-xs-7 text-right">
                <h6 class="login-link"></h6>
                <a class="menu-link">
                    <span class="icon icon-navicon-round"></span>
                </a>
                <a class="login-link-mobile">
                    <span class="icon icon-person"></span>
                </a>
                <ul class="menu">
                    <li><a href="<?= base_url() ?>#how-we-help-you">How We Help You</a></li>
                    <li><a href="<?= base_url() ?>#why-us">Why Us</a></li>
                    <li><a href="<?= base_url() ?>#features">Features</a></li>
                    <li><a href="<?= base_url() ?>#our-team">Our Team</a></li>
                    <li><a href="<?= base_url() ?>#pricing">Pricing</a></li>
                    <li class="menu-external"><a href="<?= base_url() ?>insights">Related Insights</a></li>
                    <li class="promo header-btn"><a href="<?= base_url() ?>#free-trial">TRY FOR FREE</a></li>
                    <li class="promo1 header-btn"><a href="https://socmed.udata.id/udata">LOGIN</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
