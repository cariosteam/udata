<!-- footer -->
<footer class="bg-success">
    <div class="container">
        <div class="row top-xs">
            <div class="col-sm-8 col-xs-12">
                <div class="row middle-xs start-sm center-xs">
                    <img class="logo-footer" src="<?= asset_url() ?>img/logo-udata-monochrome-light.png"/>
                    <h5 class="brand-caption">Social Media<br/> Analytics</h5>
                </div>
                <br/>
                <p>
                    &copy; Copyrights Telkom Indonesia 2015 - 2016. <br/>
                    All Rights Reserved
                </p>
            </div>
            <div class="col-sm-4 col-xs-12">
                <p>
                    Divisi Digital Telkom Indonesia<br/>
                    <?= $contact->address ?><br/>
                    <?= $contact->city ?>&nbsp;<?= $contact->zipcode ?>
                </p>
                <p>
                    Tel : <?= $contact->phone ?> <br/>
                    Fax : <?= $contact->fax ?>
                </p>
            </div>
        </div>
        <div class="row middle-xs">
            <div class="col-sm-4 col-xs-12 last-sm">
                <ul class="social-link">
                    <li><a href="http://www.facebook.com/<?= $contact->facebook ?>"><span class="icon icon-social-facebook"></span></a></li>
                    <li><a href="http://www.twitter.com/<?= $contact->twitter ?>"><span class="icon icon-social-twitter"></span></a></li>
                    <li><a href="http://www.linkedin.com/<?= $contact->linkedin ?>"><span class="icon icon-social-linkedin"></span></a></li>
                </ul>
            </div>
            <div class="col-sm-8 col-xs-12">
                <p>
                    <a href="<?= base_url() ?>#free-trial">Contact Us</a> |
                    <a href="<?= base_url() ?>page/term-of-use">Term Of Use</a> |
                    <a href="<?= base_url() ?>page/data-policy">Data Policy</a> |
                    <a href="<?= base_url() ?>faq">FAQ</a> |
                    <a href="<?= base_url() ?>page/about">About</a>
                </p>
            </div>
        </div>
    </div>
</footer>
