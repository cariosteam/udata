<a href="<?= base_url() ?>pricing/create" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Plan
</a>
<?php if(empty($plans)): ?>
<h2 class="text-muted">pricing plan is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>name</th>
            <th>color</th>
            <th width="50%">spesification</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($plans as $plan): ?>
            <tr>
                <td><?= $plan->name ?></td>
                <td style="color:<?= $plan->color ?>"><?= $plan->color ?></td>
                <td>
                    <dl class="dl-horizontal">
                        <?php foreach ($plan->spesifications as $spec => $value) : ?>
                        <dt><?= $spec ?></dt>
                        <dd><?= $value ?></dd>
                        <?php endforeach; ?>
                    </dl>
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>pricing/update/<?= $plan->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>pricing/delete/<?= $plan->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
