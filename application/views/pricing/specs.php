<a href="<?= base_url() ?>pricing/createspec" class="btn btn-default btn-labeled">
    <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>
    Create New Specification
</a>
<?php if(empty($specs)): ?>
<h2 class="text-muted">slide is empty</h2>
<?php else: ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <th>name</th>
            <th width="30%">title</th>
            <th>group</th>
            <th width="15%">&nbsp;</th>
        </thead>
        <tbody>
            <?php foreach ($specs as $spec): ?>
            <tr>
                <td><code><?= $spec->name ?></code></td>
                <td><?= $spec->title ?></td>
                <td><?= $spec->group->name ?></td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm">
                        <a href="<?= base_url() ?>pricing/updatespec/<?= $spec->id ?>" class="btn btn-default" data-toggle="tooltip" title="update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?= base_url() ?>pricing/deletespec/<?= $spec->id ?>" class="btn btn-danger" data-toggle="tooltip" title="remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
