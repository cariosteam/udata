<?= form_open(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $errors ?>
        </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
        <?= form_label('Name','name') ?>
        <?= form_input(array(
            'name' => 'name',
            'id' => 'name',
            'maxLength' => '50',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
        <div class="form-group">
            <?= form_label('Group','group') ?>
            <select class="form-control" name="group">
                <?php foreach ($groups as $group): ?>
                <option value=<?= $group->id ?> ><?= $group->name?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
        <?= form_label('Title','title') ?>
        <?= form_input(array(
            'name' => 'title',
            'id' => 'title',
            'maxLength' => '100',
            'required' => 'true',
            'class' => 'form-control'
        )) ?>
        </div>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Specification
    </button>
</div>

<?= form_close() ?>
