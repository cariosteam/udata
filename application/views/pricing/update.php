<?= form_open(current_url()) ?>

<div class="row">
    <div class="col-sm-12">
        <?php if(!empty($errors)): ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $errors ?>
        </div>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
        <?= form_label('Name','name') ?>
        <?= form_input(array(
            'name' => 'name',
            'id' => 'name',
            'maxLength' => '50',
            'required' => 'true',
            'class' => 'form-control'
        ), $plan->name) ?>
        </div>
        <div class="form-group">
            <?= form_label('Color','color') ?>
            <div class="input-group colorpicker-input">
                <?= form_input(array(
                    'name' => 'color',
                    'id' => 'color',
                    'maxLength' => '20',
                    'required' => 'true',
                    'class' => 'form-control'
                ), $plan->color) ?>
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
        <?php foreach ($specs as $spec): ?>
            <?= form_label($spec->name, $spec->name) ?>
            <?= form_input(array(
                'name' => $spec->name,
                'id' => $spec->name,
                'maxLength' => '50',
                'required' => 'true',
                'class' => 'form-control'
            ), $plan->spesifications->{$spec->name} ) ?>
        <?php endforeach; ?>
    </div>
</div>

<hr/>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-labeled">
        <span class="btn-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>
        Save Plan
    </button>
</div>

<?= form_close() ?>
