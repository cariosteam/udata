<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MY_Controller {
    protected $models = array('team');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of team profile';
        $this->data['teams'] = $this->team->get_all();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['team'] = $this->team->get($id);
        if(!$this->data['team']) show_404();
        $this->data['title'] = 'team profile of '.$this->data['team']->name;
    }

    public function create(){
        $this->data['title'] = 'create new team profile';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $this->load->library('my_upload');
        $this->my_upload->upload($_FILES['photo']);

        // upload image first
        $image = uniqid('team_');
        if(!$this->my_upload->uploaded){
            $this->data['errors'] = $this->my_upload->error;
        } else {
            $this->my_upload->allowed            = array('image/*');
            $this->my_upload->image_convert      = 'jpg';
            $this->my_upload->file_max_size      = 25000000;
            $this->my_upload->file_new_name_body = $image;
            $this->my_upload->image_resize       = true;
            $this->my_upload->image_ratio_crop   = true;
            $this->my_upload->image_x            = 240;
            $this->my_upload->image_y            = 240;

            $this->my_upload->process('./uploads/');

            if ( !$this->my_upload->processed ) {
                // error
                $this->data['errors'] = $this->my_upload->error;
            } else {
                $this->my_upload->clean();
                $id = $this->team->insert(array(
                    'name' => $this->input->post('name'),
                    'link' => $this->input->post('link'),
                    'profile' => $this->input->post('profile'),
                    'photo' => $this->my_upload->file_dst_name
                ));

                if(!$id){
                    $this->data['error'] = "error insert record";
                }else{
                    redirect('/team/show/'.$id);
                }
            }
        }
    }

    public function update($id){
        if(!$id) show_404();
        $this->data['team'] = $this->team->get($id);
        if(!$this->data['team']) show_404();
        $this->data['title'] = 'team profile of '.$this->data['team']->name;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // upload image first
        if($_FILES['photo']['tmp_name']!=='') {
            $this->load->library('my_upload');
            $this->my_upload->upload($_FILES['photo']);

            // upload image first
            $image = uniqid('team_');
            if(!$this->my_upload->uploaded){
                $this->data['errors'] = $this->my_upload->error;
                return;
            } else {
                $this->my_upload->allowed            = array('image/*');
                $this->my_upload->image_convert      = 'jpg';
                $this->my_upload->file_max_size      = 25000000;
                $this->my_upload->file_new_name_body = $image;
                $this->my_upload->image_resize       = true;
                $this->my_upload->image_ratio_crop   = true;
                $this->my_upload->image_x            = 240;
                $this->my_upload->image_y            = 240;

                $this->my_upload->process('./uploads/');

                if ( !$this->my_upload->processed ) {
                    // error
                    $this->data['errors'] = $this->my_upload->error;
                    return;
                } else {
                    $this->my_upload->clean();
                    $team = $this->team->update(array(
                        'name' => $this->input->post('name'),
                        'link' => $this->input->post('link'),
                        'profile' => $this->input->post('profile'),
                        'photo' => $this->my_upload->file_dst_name
                    ), $id);
                }
            }
        } else {
            $team = $this->team->update(array(
                'name' => $this->input->post('name'),
                'link' => $this->input->post('link'),
                'profile' => $this->input->post('profile')
            ), $id);
        }

        if(!$team){
            $this->data['error'] = "error update record";
            return;
        }else{
            redirect('/team/show/'.$id);
        }
    }

    public function delete($id){
        $this->view = FALSE;
        $team = $this->team->delete($id);
        if(!$team){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/team');
        }
    }
}
?>
