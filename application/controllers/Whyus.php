<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whyus extends MY_Controller {
    protected $models = array('why_us');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of why us slider';
        $this->data['slides'] = $this->why_us->get_all();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['slide'] = $this->why_us->get($id);
        if(!$this->data['slide']) show_404();
        $this->data['title'] = 'slider '.$this->data['slide']->title;
    }

    public function create(){
        $this->data['title'] = 'create new why us slider';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $this->load->library('my_upload');
        $this->my_upload->upload($_FILES['image']);

        // upload image first
        $image = uniqid('whyus_');
        if(!$this->my_upload->uploaded){
            $this->data['errors'] = $this->my_upload->error;
        } else {
            $this->my_upload->allowed            = array('image/*');
            $this->my_upload->image_convert      = 'jpg';
            $this->my_upload->file_max_size      = 25000000;
            $this->my_upload->file_new_name_body = $image;
            $this->my_upload->image_resize       = true;
            $this->my_upload->image_ratio_crop   = true;
            $this->my_upload->image_x            = 309;
            $this->my_upload->image_y            = 191;

            $this->my_upload->process('./uploads/');

            if ( !$this->my_upload->processed ) {
                // error
                $this->data['errors'] = $this->my_upload->error;
            } else {
                $this->my_upload->clean();
                $id = $this->why_us->insert(array(
                    'title' => $this->input->post('title'),
                    'caption' => $this->input->post('caption'),
                    'image' => $this->my_upload->file_dst_name
                ));

                if(!$id){
                    $this->data['error'] = "error insert record";
                }else{
                    redirect('/whyus/show/'.$id);
                }
            }
        }
    }

    public function update($id){
        $this->data['title'] = 'update why us slider '.$id;
        if(!$id) show_404();
        $this->data['slide'] = $this->why_us->get($id);
        if(!$this->data['slide']) show_404();
        $this->data['title'] = 'slider '.$this->data['slide']->title;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // upload image first
        if($_FILES['image']['tmp_name']!=='') {
            $this->load->library('my_upload');
            $this->my_upload->upload($_FILES['image']);

            // upload image first
            $image = uniqid('whyus_');
            if(!$this->my_upload->uploaded){
                $this->data['errors'] = $this->my_upload->error;
                return;
            } else {
                $this->my_upload->allowed            = array('image/*');
                $this->my_upload->image_convert      = 'jpg';
                $this->my_upload->file_max_size      = 25000000;
                $this->my_upload->file_new_name_body = $image;
                $this->my_upload->image_resize       = true;
                $this->my_upload->image_ratio_crop   = true;
                $this->my_upload->image_x            = 309;
                $this->my_upload->image_y            = 191;

                $this->my_upload->process('./uploads/');

                if ( !$this->my_upload->processed ) {
                    // error
                    $this->data['errors'] = $this->my_upload->error;
                    return;
                } else {
                    $this->my_upload->clean();
                    $slide = $this->why_us->update(array(
                        'title' => $this->input->post('title'),
                        'caption' => $this->input->post('caption'),
                        'image' => $this->my_upload->file_dst_name
                    ), $id);
                }
            }
        } else {
            $slide = $this->why_us->update(array(
                'title' => $this->input->post('title'),
                'caption' => $this->input->post('caption'),
                'content' => $this->input->post('content')
            ), $id);
        }

        if(!$slide){
            $this->data['error'] = "error update record";
            return;
        }else{
            redirect('/whyus/show/'.$id);
        }
    }

    public function delete($id){
        $this->view = FALSE;
        $slide = $this->why_us->delete($id);
        if(!$slide){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/whyus');
        }
    }
}
?>
