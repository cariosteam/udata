<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trial extends MY_Controller {
        protected $models = array('trial_request');
        protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of trial request';
        $this->data['trials'] = $this->trial_request->get_all();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['trial'] = $this->trial_request->get($id);
        if(!$this->data['trial']) show_404();
        $this->data['title'] = 'trial request #'.$this->data['trial']->id;
    }
    public function _set_rules(){
        $this->load->library('form_validation');
        $this->middle = 'index'; 
        
        // Displaying Errors In Div
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // Validation For Name Field
        $this->form_validation->set_rules('firstName', 'firstName', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[trial_request.email');
        $this->form_validation->set_rules('phone', 'trim|required|regex_match[/^[0-9]{10}$/]');
        
        
    }
}
?>
