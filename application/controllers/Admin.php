<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
    protected $models = array('admin');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'Administrator';
        $query = $this->db->get($this->admin->table, 1);
        $admin = $query->row();

        // create admin record if not exist
        if(empty($admin)){
            $id = $this->admin->insert(array(
                'name' => 'admin',
                'password' => 'adminpasswd'
            ));
            $this->data['admin'] = $this->admin->get($id);
        } else {
            $id = $admin->id;
            $this->data['admin'] = $admin;
        }

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $updated_admin = $this->admin->update(array(
            'name' => $this->input->post('name'),
            'password' => $this->input->post('password')
        ),$id);

        if(!$updated_admin){
            $this->data['error'] = "error update record";
        } else {
            $this->data['admin'] = $this->admin->get($id);
        }
    }
}
?>
