<?php


class Faq_category extends MY_Controller{

    protected $models = array('faq_kategori','faq');
    protected $layout = 'layouts/dashboard';

    function __construct(){
        parent::__construct();
        $this->load->model('Faq_kategori_model');
        $this->load->model('Faq_model');
    }

    function index(){
        $this->data['title'] = 'list of Faq Category';
        $this->data['faq'] = $this->Faq_kategori_model->listAllCategory();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['faq'] = $this->Faq_kategori_model->getById($id)[0];
        if(!$this->data['faq']) show_404();
        $this->data['title'] = 'Faq Category of '.$this->data['faq']->category;
    }

    public function create(){
        $this->data['title'] = 'create new faq';
        $this->data['kategori'] = $this->Faq_kategori_model->listAllCategory();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $id = $this->Faq_kategori_model->addNewFaqCategory(array(
            'category' => $this->input->post('category'),
            'description' => $this->input->post('description'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'alias' => strtolower(str_replace(' ','-',$this->input->post('category')))
        ));

        if(!$id){
            $this->data['error'] = "error insert record";
        }else{
            redirect('/faq/category/show/'.$id);
        }


    }

    public function update($id){
        if(!$id) show_404();
        $this->data['category'] = $this->Faq_kategori_model->getById($id)[0];
        if(!$this->data['category']) show_404();
        $this->data['title'] = 'Faq Category of '.$this->data['category']->category;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $faq = $this->Faq_kategori_model->updateFaqCategory(array(
            'category' => $this->input->post('category'),
            'description' => $this->input->post('description'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'alias' => strtolower(str_replace(' ','-',$this->input->post('category')))
        ),$id);


        if(!$faq){
            $this->data['error'] = "error update record";
            return;
        }else{
            redirect('/faq/show/'.$id);
        }
    }

    public function delete($id){
        $this->view = FALSE;
        $feature = $this->Faq_kategori_model->delete($id);
        if(!$feature){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/faq/category');
        }
    }
}