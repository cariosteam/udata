<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {
    protected $models = array('page', 'contact');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of page';
        $this->data['pages'] = $this->page->get_all();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['page'] = $this->page->get($id);
        if(!$this->data['page']) show_404();

        $this->data['title'] = 'page of '.$this->data['page']->title;
    }

    public function view($slug){
        if(!$slug) show_404();
        $this->layout = 'layouts/page';
        $this->data['page'] = $this->page->get(array('slug'=>$slug));
        if(!$this->data['page']) show_404();

        $this->data['title'] = 'page of '.$this->data['page']->title;
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();
    }

    public function create(){
        $this->data['title'] = 'create new page';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $slug = !empty($this->input->post('slug'))?
            $this->input->post('slug'):
            url_title($this->input->post('title'),'-', true);
        $id = $this->page->insert(array(
            'title' => $this->input->post('title'),
            'subtitle' => $this->input->post('subtitle'),
            'slug' => $slug,
            'content' => $this->input->post('content')
        ));

        if(!$id){
            $this->data['error'] = "error insert record";
        }else{
            redirect('/page/show/'.$id);
        }
    }

    public function update($id){
        if(!$id) show_404();
        $this->data['page'] = $this->page->get($id);
        if(!$this->data['page']) show_404();
        $this->data['title'] = 'page of '.$this->data['page']->title;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $slug = !empty($this->input->post('slug'))?
            $this->input->post('slug'):
            url_title($this->input->post('title'),'-', true);
        $page = $this->page->update(array(
            'title' => $this->input->post('title'),
            'subtitle' => $this->input->post('subtitle'),
            'slug' => $slug,
            'content' => $this->input->post('content')
        ), $id);

        if(!$page){
            $this->data['error'] = "error insert record";
        }else{
            redirect('/page/show/'.$id);
        }
    }

    public function delete($id){
        $this->view = FALSE;
        $page = $this->page->delete($id);
        if(!$page){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/page');
        }
    }
}
?>
