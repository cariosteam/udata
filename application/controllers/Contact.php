<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {
    protected $models = array('contact');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'Address and Contact';
        $query = $this->db->get($this->contact->table, 1);
        $contact = $query->row();

        // create contact record if not exist
        if(empty($contact)){
            $id = $this->contact->insert(array(
                'address' => 'default',
            ));
            $this->data['contact'] = $this->contact->get($id);
        } else {
            $id = $contact->id;
            $this->data['contact'] = $contact;
        }

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $updated_contact = $this->contact->update(array(
            'address' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'zipcode' => $this->input->post('zipcode'),
            'phone' => $this->input->post('phone'),
            'fax' => $this->input->post('fax'),
            'state' => $this->input->post('state'),
            'twitter' => $this->input->post('twitter'),
            'facebook' => $this->input->post('facebook'),
            'linkedin' => $this->input->post('linkedin'),
            'lat' => $this->input->post('lat'),
            'long' => $this->input->post('long')
        ),$id);

        if(!$updated_contact){
            $this->data['error'] = "error update record";
            return;
        }
        $this->data['contact'] = $this->contact->get($id);
    }
}
?>
