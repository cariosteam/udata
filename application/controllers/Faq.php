<?php


class Faq extends MY_Controller{
    protected $models = array('faq_kategori','faq');

    function __construct(){
        parent::__construct();
        //constructor
        //all module needed load in here
        $this->load->model('Faq_kategori_model');
        $this->load->model('Faq_model');
    }

    /*function index(){
        $this->data['data'] = $this->Faq_kategori_model->listAllCategory();
        $this->data['title'] = "Frequently Asked Question";

    }*/

    function faq_list(){
        $this->data['title'] = 'list of Faq';
        $this->data['faq'] = $this->Faq_model->listAllFaq();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['faq'] = $this->Faq_model->getById($id)[0];
        if(!$this->data['faq']) show_404();
        $this->data['title'] = 'feature of '.$this->data['faq']->title;
    }

    public function create(){
        $this->data['title'] = 'create new faq';
        $this->data['kategori'] = $this->Faq_kategori_model->listAllCategory();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $id = $this->Faq_model->addNewFaq(array(
            'title' => $this->input->post('title'),
            'id_category' => $this->input->post('category'),
            'question' => $this->input->post('question'),
            'answer' => $this->input->post('jawaban'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'alias' => strtolower(str_replace(' ','-',$this->input->post('title')))
        ));

        if(!$id){
            $this->data['error'] = "error insert record";
        }else{
            redirect('/faq/show/'.$id);
        }


    }

    public function update($id){
        if(!$id) show_404();
        $this->data['kategori'] = $this->Faq_kategori_model->listAllCategory();
        $this->data['faq'] = $this->Faq_model->getById($id)[0];
        if(!$this->data['faq']) show_404();
        $this->data['title'] = 'Faq of '.$this->data['faq']->title;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $faq = $this->Faq_model->updateFaq(array(
            'title' => $this->input->post('title'),
            'id_category' => $this->input->post('category'),
            'question' => $this->input->post('question'),
            'answer' => $this->input->post('answer'),
            'updated_at' => date('Y-m-d H:i:s'),
            'alias' => strtolower(str_replace(' ','-',$this->input->post('title')))
        ),$id);


        if(!$faq){
            $this->data['error'] = "error update record";
            return;
        }else{
            redirect('/faq/show/'.$id);
        }
    }

    function faqlist(){
        $data = array(
            'title' => "Frequently Asked Question",
            'layout' =>'layouts/dashboard',
            'faq'=>$this->faq->listAllFaq()
        );
        $this->load->view('faq/index',$data);
    }

    public function delete($id){
        $this->view = FALSE;
        $feature = $this->Faq_model->delete($id);
        if(!$feature){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/faq/faq_list');
        }
    }
}