<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    protected $models = array('admin');

    public function index(){
        $this->layout = 'layouts/dashboard';
        // redirect unauthenticated user
        if(!$this->admin->isAuthenticated())
            redirect('/dashboard/login');
        redirect('/howhelp');
    }

    public function login(){
        // init
        $this->load->library('form_validation');
        $this->layout = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'name',
                'label' => 'Username',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'You must provide a %s.',
                ),
            )
        ));

        if ($this->form_validation->run() === FALSE){
            // do nothing
        } else {
            // authorize
            $admin = $this->admin->auth();
            if(!$admin) {
                $this->data['unauthorize'] = TRUE;
            } else {
                redirect('/dashboard/index');
            }

        }
    }

    public function logout(){
        $this->view = FALSE;
        $this->admin->logout();
        redirect('/dashboard/login');
    }
}
?>
