<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends MY_Controller {
    protected $models = array(
        'contact',
        'feature',
        'help_slider',
        'pricing_plan',
        'pricing_plan_spec',
        'pricing_spec',
        'pricing_spec_group',
        'team',
        'trial_request',
        'why_us'
    );
    protected $layout = 'layouts/home';

    public function index(){
        $this->data['slides'] = $this->help_slider->get_all();
        $this->data['whys'] = $this->why_us->get_all();
        $this->data['teams'] = $this->team->get_all();
        $this->data['features'] = $this->feature->get_all();
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();

        // pricing
        $plans = $this->pricing_plan->with_specs()->get_all();
        $specGroups = $this->pricing_spec_group->with_specs()->get_all();
        $this->data['pricing_specs'] = $specGroups;

        // add spec as member of plans
        $this->data['pricing_plans'] = array_map(function ($plan) use ($specGroups){
            // inititate specgroup data holder, group by spec group
            $plan->spesifications = new stdClass();
            foreach ($specGroups as $specGroup) {
                // inititate spec hold
                $plan->spesifications->{$specGroup->name} = new stdClass();
                // iterate all specs in spec groups
                foreach ($specGroup->specs as $spec) {
                    $spec_index = -1;
                    $spec_index = array_search($spec->id, array_map(function($p_spec){
                        return $p_spec->pricing_spec;
                    }, $plan->specs));
                    $plan->spesifications->{$specGroup->name}->{$spec->name} =  ($spec_index > -1)? $plan->specs[$spec_index]->spec:NULL;
                }
            }
            unset($plan->specs);
            return $plan;
        }, $plans);
    }

    public function faq(){
        $this->data['slides'] = $this->help_slider->get_all();
        $this->data['whys'] = $this->why_us->get_all();
        $this->data['teams'] = $this->team->get_all();
        $this->data['features'] = $this->feature->get_all();
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();

        $this->load->model('Faq_kategori_model');
        $this->load->model('Faq_model');

        $this->data['category'] = $this->Faq_kategori_model->listAllCategory();
        $this->data['title'] = "Frequently Asked Question";
    }

    public function tryproduct(){
        $this->form_validation->set_rules(array(
             array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|is_unique[trial_request.email]',
                'errors' => array(
                    'is_unique' => 'Email has already exist'
                    ),
                )
        ));

        $email = $this->input->post('email');
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'syehbiherbian@gmail.com',
        'smtp_pass' => '@syehbi25',
        'mailtype'  => 'html', 
        'charset'   => 'iso-8859-1'
    );
    
        
        $this->view = FALSE;
        // request must in POST
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') return;
        // get user data
        $data = array(
            'firstname' => $this->input->post('firstName'),
            'lastname'  => $this->input->post('lastName'),
            'position'  => $this->input->post('position'),
            'company'   => $this->input->post('company'),
            'address'   => $this->input->post('address'),
            'phone'     => $this->input->post('phone'),
            'email'     => $email,
            'notes'     => $this->input->post('notes')
        );
        if($this->form_validation->run() != FALSE){
            $id = $this->trial_request->insert($data);
            echo json_encode(array(
                'error' => false,
                'message' => '<h2><b>Success !</b></h2>Success register product trial!',
                'user' => $data,
            ));
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('syehbiherbian@gmail.com', 'Syehbi');
        $this->email->to($email);
        $this->email->subject('Registration Verification:');
        $message = "Thanks for signing up! Your account has been created...!";
        $this->email->message($message);
        if ( ! $this->email->send()) {
            show_error($this->email->print_debugger());
        } 
        
        }else{
            echo json_encode(array(
                'error' => true,
                'message' => "<h2><b>Failed !</b></h2> ".validation_errors().""
            ));
       }
        
        
            
        }

        }      
?>
