<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature extends MY_Controller {
    protected $models = array('feature');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of feature';
        $this->data['features'] = $this->feature->get_all();
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['feature'] = $this->feature->get($id);
        if(!$this->data['feature']) show_404();
        $this->data['title'] = 'feature of '.$this->data['feature']->name;
    }

    public function create(){
        $this->data['title'] = 'create new feature';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $this->load->library('my_upload');
        $this->my_upload->upload($_FILES['illustration']);

        // upload image first
        $image = uniqid('feature_');
        if(!$this->my_upload->uploaded){
            $this->data['errors'] = $this->my_upload->error;
        } else {
            $this->my_upload->allowed            = array('image/*');
            $this->my_upload->image_convert      = 'jpg';
            $this->my_upload->file_max_size      = 25000000;
            $this->my_upload->file_new_name_body = $image;
            $this->my_upload->image_resize       = true;
            $this->my_upload->image_ratio_crop   = true;
            $this->my_upload->image_x            = 395;
            $this->my_upload->image_y            = 222;

            $this->my_upload->process('./uploads/');

            if ( !$this->my_upload->processed ) {
                // error
                $this->data['errors'] = $this->my_upload->error;
            } else {
                $this->my_upload->clean();
                $id = $this->feature->insert(array(
                    'title' => $this->input->post('title'),
                    'name' => $this->input->post('name'),
                    'content' => $this->input->post('content'),
                    'illustration' => $this->my_upload->file_dst_name
                ));

                if(!$id){
                    $this->data['error'] = "error insert record";
                }else{
                    redirect('/feature/show/'.$id);
                }
            }
        }
    }

    public function update($id){
        if(!$id) show_404();
        $this->data['feature'] = $this->feature->get($id);
        if(!$this->data['feature']) show_404();
        $this->data['title'] = 'feature of '.$this->data['feature']->name;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // upload image first
        if($_FILES['illustration']['tmp_name']!=='') {
            $this->load->library('my_upload');
            $this->my_upload->upload($_FILES['illustration']);

            // upload image first
            $image = uniqid('feature_');
            if(!$this->my_upload->uploaded){
                $this->data['errors'] = $this->my_upload->error;
                return;
            } else {
                $this->my_upload->allowed            = array('image/*');
                $this->my_upload->image_convert      = 'jpg';
                $this->my_upload->file_max_size      = 25000000;
                $this->my_upload->file_new_name_body = $image;
                $this->my_upload->image_resize       = true;
                $this->my_upload->image_ratio_crop   = true;
                $this->my_upload->image_x            = 395;
                $this->my_upload->image_y            = 222;

                $this->my_upload->process('./uploads/');

                if ( !$this->my_upload->processed ) {
                    // error
                    $this->data['errors'] = $this->my_upload->error;
                    return;
                } else {
                    $this->my_upload->clean();
                    $feature = $this->feature->update(array(
                        'title' => $this->input->post('title'),
                        'name' => $this->input->post('name'),
                        'content' => $this->input->post('content'),
                        'illustration' => $this->my_upload->file_dst_name
                    ), $id);
                }
            }
        } else {
            $feature = $this->feature->update(array(
                'title' => $this->input->post('title'),
                'name' => $this->input->post('name'),
                'content' => $this->input->post('content'),
            ), $id);
        }

        if(!$feature){
            $this->data['error'] = "error update record";
            return;
        }else{
            redirect('/feature/show/'.$id);
        }
    }

    public function delete($id){
        $this->view = FALSE;
        $feature = $this->feature->delete($id);
        if(!$feature){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/feature');
        }
    }
}
?>
