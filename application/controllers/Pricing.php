<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing extends MY_Controller {
    protected $models = array(
        'pricing_plan',
        'pricing_plan_spec',
        'pricing_spec',
        'pricing_spec_group'
    );
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of pricing plan';
        $plans = $this->pricing_plan->with_specs()->get_all();
        $specs = $this->pricing_spec->with_group()->get_all();

        // add spec as member of plans
        $this->data['plans'] = array_map(function ($plan) use ($specs){
            $plan->spesifications = new stdClass();
            foreach ($specs as $spec) {
                $spec_index = -1;
                $spec_index = array_search($spec->id, array_map(function($p_spec){
                    return $p_spec->pricing_spec;
                }, $plan->specs));

                $plan->spesifications->{$spec->name} = ($spec_index > -1)? $plan->specs[$spec_index]->spec:NULL;
            }
            unset($plan->specs);
            return $plan;
        }, $plans);
    }

    public function create(){
        $this->data['title'] = 'create new pricing plan';
        // get all active specs
        $this->data['specs'] =  $this->pricing_spec->with_group()->get_all();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $plan_id = $this->pricing_plan->insert(array(
            'name' => $this->input->post('name'),
            'color' => $this->input->post('color')
        ));
        if(!$plan_id){
            return $this->data['error'] = "error insert plan record";
        }

        // create plan spec as member of new plan
        foreach ($this->data['specs'] as $spec) {
            if(!empty($this->input->post($spec->name))){
                $spec_id = $this->pricing_plan_spec->insert(array(
                    'spec' => $this->input->post($spec->name),
                    'pricing_spec' => $spec->id,
                    'pricing_plan' => $plan_id
                ));
                if(!$spec_id){
                    return $this->data['error'] = "error insert spec ".$spec->name." record on plan ".$plan_id;
                }
            }
        }

        redirect('/pricing/index');
    }

    public function update($id){
        if(!$id) show_404();
        $plan = $this->pricing_plan->with_specs()->get($id);
        if(!$plan) show_404();
        $this->data['title'] = 'pricing plan '.$plan->name;
        $specs = $this->pricing_spec->with_group()->get_all();

        $plan->spesifications = new stdClass();
        foreach ($specs as $spec) {
            $spec_index = -1;
            $spec_index = array_search($spec->id, array_map(function($p_spec){
                return $p_spec->pricing_spec;
            }, $plan->specs));

            $plan->spesifications->{$spec->name} = ($spec_index > -1)? $plan->specs[$spec_index]->spec:NULL;
        }

        // pass to view
        $this->data['plan'] = $plan;
        $this->data['specs'] =  $specs;

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        $this->pricing_plan->update(array(
            'name' => $this->input->post('name'),
            'color' => $this->input->post('color')
        ), $id);

        // update plan specs
        foreach ($specs as $spec) {
            if(!empty($this->input->post($spec->name))){
                $spec_index = -1;
                $spec_index = array_search($spec->id, array_map(function($p_spec){
                    return $p_spec->pricing_spec;
                }, $plan->specs));

                // spec not defined on this plan
                if($spec_index > -1){
                    $this->pricing_plan_spec->update(array(
                        'spec' => $this->input->post($spec->name)
                    ), $plan->specs[$spec_index]->id);
                }else{
                    $this->pricing_plan_spec->insert(array(
                        'spec' => $this->input->post($spec->name),
                        'pricing_spec' => $spec->id,
                        'pricing_plan' => $plan->id
                    ));
                }
            }
        }
        redirect('pricing/index');
    }

    public function delete($id){
        $this->view = FALSE;
        $plan = $this->pricing_plan->with_specs()->get($id);
        if(!$plan) show_404();
        $specs = $this->pricing_spec->with_group()->get_all();

        // delete all plan specs before delete plan
        foreach ($specs as $spec) {
            $spec_index = array_search($spec->id, array_map(function($p_spec){
                return $p_spec->pricing_spec;
            }, $plan->specs));
            $this->pricing_plan_spec->delete($plan->specs[$spec_index]->id);
        }

        // now delete plan
        $deleted_plan = $this->pricing_plan->delete($id);
        if(!$deleted_plan){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/pricing');
        }
    }

    public function specs(){
        $this->data['title'] = 'list of pricing specification';
        $this->data['specs'] = $this->pricing_spec->with_group()->get_all();
    }

    public function createspec(){
        $this->data['title'] = 'create new pricing specification';
        $this->data['groups'] = $this->pricing_spec_group->get_all();

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }
        $spec = $this->pricing_spec->insert(array(
            'name' => str_replace(' ', '_', $this->input->post('name')),
            'title' => $this->input->post('title'),
            'group' => $this->input->post('group')
        ));
        if(!$spec){
            return $this->data['error'] = "error insert plan record";
        }
        redirect('pricing/specs');
    }

    public function updatespec($id){
        if(!$id) show_404();
        $this->data['spec'] = $this->pricing_spec->with_group()->get($id);
        $this->data['groups'] = $this->pricing_spec_group->get_all();

        if(!$this->data['spec']) show_404();
        $this->data['title'] = 'update '.$this->data['spec']->name.' specification';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }
        $spec = $this->pricing_spec->update(array(
            'name' => str_replace(' ', '_', $this->input->post('name')),
            'title' => $this->input->post('title'),
            'group' => $this->input->post('group')
        ), $id);
        if(!$spec){
            return $this->data['error'] = "error insert plan record";
        }
        redirect('pricing/specs');
    }

    public function deletespec($id){
        $this->view = FALSE;
        // delete all related plan spec before delete specs
        $this->pricing_plan_spec->delete(array('pricing_spec' => $id));

        // now delete spec
        $spec = $this->pricing_spec->delete($id);
        if(!$spec){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/pricing/specs');
        }
    }

    public function specgroups(){
        $this->data['title'] = 'list of pricing specification group';
        $this->data['groups'] = $this->pricing_spec_group->get_all();
    }

    public function createspecgroup(){
        $this->data['title'] = 'create new pricing specification group';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }
        $group = $this->pricing_spec_group->insert(array(
            'name' => $this->input->post('name')
        ));
        if(!$group){
            return $this->data['error'] = "error insert plan record";
        }
        redirect('pricing/specgroups');
    }

    public function updatespecgroup($id){
        if(!$id) show_404();
        $this->data['group'] = $this->pricing_spec_group->get($id);
        if(!$this->data['group']) show_404();
        $this->data['title'] = 'update '.$this->data['group']->name.' specification group';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }
        $group = $this->pricing_spec_group->update(array(
            'name' => $this->input->post('name')
        ), $id);
        if(!$group){
            return $this->data['error'] = "error insert plan record";
        }
        redirect('pricing/specgroups');
    }

    public function deletespecgroup($id){
        $this->view = FALSE;
        $group = $this->pricing_spec_group->with_specs()->get($id);
        // delete all plan spec before delete all spec
        foreach ($group->specs as $spec) {
            $this->pricing_plan_spec->delete(array('pricing_spec' => $spec->id));
        }
        // now delete spec
        $this->pricing_spec->delete(array('group' => $id));
        // delete group
        $deleted_group = $this->pricing_spec_group->delete($id);
        if(!$deleted_group){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/pricing/specgroups');
        }
    }
}
?>
