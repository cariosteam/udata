<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication extends MY_Controller {
    protected $models = array(
        'publication',
        'publication_reader',
        'contact');
    protected $layout = 'layouts/dashboard';

    public function index(){
        $this->data['title'] = 'list of publication';
        $this->data['publications'] = $this->publication->with_readers('fields:*count*')->get_all();
    }

    public function entries($page = 1){
        $this->layout = 'layouts/publication';
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();
        $this->data['number_of_entry'] = $this->publication->count_rows();

        // pagination helpers
        $this->data['publications'] = $this->publication
            ->paginate(10, $this->data['number_of_entry'], $page);
        $this->data['all_pages'] = $this->publication->all_pages;
        $this->data['previous_page'] = $this->publication->previous_page;
        $this->data['next_page'] = $this->publication->next_page;
    }

    public function view($id){
        if(!$id) show_404();
        $this->layout = 'layouts/publication';
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();
        $this->data['publication'] = $this->publication->get($id);
        if(!$this->data['publication']) show_404();
    }

    public function download($id){
        if(!$id) show_404();
        $this->layout = 'layouts/publication';
        $this->data['contact'] = $this->db->get($this->contact->table, 1)->row();
        $this->data['publication'] = $this->publication->get($id);
        if(!$this->data['publication']) show_404();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // download file
        // record downloader
        $id = $this->publication_reader->insert(array(
            'firstname' => $this->input->post('firstName'),
            'lastname' => $this->input->post('lastName'),
            'position' => $this->input->post('position'),
            'company' => $this->input->post('company'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'publication' => $id
        ));

        if(!$id){
            $this->data['error'] = "error insert record";
            return;
        }

        // download file
        $file = './publications/'.$this->data['publication']->file;

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function show($id){
        if(!$id) show_404();
        $this->data['publication'] = $this->publication->with_readers('fields:*count*')->get($id);
        if(!$this->data['publication']) show_404();
        $this->data['title'] = 'publication of '.$this->data['publication']->title;
    }

    public function create(){
        $this->data['title'] = 'create new publication';
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // get post data
        $publication_data = array(
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'content' => $this->input->post('content')
        );

        // upload multipart section
        $this->load->library('my_upload');

        // upload cover first
        $this->my_upload->upload($_FILES['cover']);
        $image = uniqid('publication_');
        if(!$this->my_upload->uploaded){
            $this->data['errors'] = $this->my_upload->error;
            return;
        }
        $this->my_upload->allowed            = array('image/*');
        $this->my_upload->image_convert      = 'jpg';
        $this->my_upload->file_max_size      = 25000000;
        $this->my_upload->file_new_name_body = $image;
        $this->my_upload->image_resize       = true;
        $this->my_upload->image_ratio_crop   = true;
        $this->my_upload->image_x            = 400;
        $this->my_upload->image_y            = 560;

        $this->my_upload->process('./uploads/');

        if ( !$this->my_upload->processed ) {
            // error
            $this->data['errors'] = $this->my_upload->error;
            return;
        }
        $publication_data['cover'] = $this->my_upload->file_dst_name;
        $this->my_upload->clean();

        // upload file
        $this->my_upload->upload($_FILES['file']);
        $pub_file = uniqid('publication_');

        if(!$this->my_upload->uploaded){
            $this->data['errors'] = $this->my_upload->error;
            return;
        }
        $this->my_upload->file_new_name_body = $pub_file;
        $this->my_upload->process('./publications/');

        // error processing upload
        if ( !$this->my_upload->processed ) {
            $this->data['errors'] = $this->my_upload->error;
            return;
        }
        $publication_data['file'] = $this->my_upload->file_dst_name;
        $this->my_upload->clean();

        // add record
        $id = $this->publication->insert($publication_data);

        if(!$id){
            $this->data['error'] = "error insert record";
            return;
        }
        redirect('/publication/show/'.$id);
    }

    public function update($id){
        if(!$id)
            show_404();
        $this->data['publication'] = $this->publication->get($id);

        if(!$this->data['publication'])
            show_404();

        $this->data['title'] = 'publication of '.$this->data['publication']->title;

        // action
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // do nothing
            return;
        }

        // get updated record from post body
        $publication_data = array(
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'content' => $this->input->post('content')
        );

        // upload image first
        if($_FILES['cover']['tmp_name']!=='') {
            $this->load->library('my_upload');
            $this->my_upload->upload($_FILES['cover']);

            // upload image first
            $image = uniqid('publication_');
            if(!$this->my_upload->uploaded){
                $this->data['errors'] = $this->my_upload->error;
                return;
            }
            $this->my_upload->allowed            = array('image/*');
            $this->my_upload->image_convert      = 'jpg';
            $this->my_upload->file_max_size      = 25000000;
            $this->my_upload->file_new_name_body = $image;
            $this->my_upload->image_resize       = true;
            $this->my_upload->image_ratio_crop   = true;
            $this->my_upload->image_x            = 400;
            $this->my_upload->image_y            = 560;

            $this->my_upload->process('./uploads/');

            if ( !$this->my_upload->processed ) {
                // error
                $this->data['errors'] = $this->my_upload->error;
                return;
            }
            $publication_data['cover'] = $this->my_upload->file_dst_name;
            $this->my_upload->clean();
        }

        // upload file
        if($_FILES['file']['tmp_name']!=='') {
            $this->my_upload->upload($_FILES['file']);
            $pub_file = uniqid('publication_');

            if(!$this->my_upload->uploaded){
                $this->data['errors'] = $this->my_upload->error;
                return;
            }

            $this->my_upload->file_new_name_body = $pub_file;
            $this->my_upload->process('./publications/');

            if ( !$this->my_upload->processed ) {
                // error
                $this->data['errors'] = $this->my_upload->error;
                return;
            }
            $publication_data['file'] = $this->my_upload->file_dst_name;
            $this->my_upload->clean();
        }

        // update publication data
        $publication = $this->publication->update($publication_data, $id);

        if(!$publication){
            $this->data['error'] = "error update record";
            return;
        }
        redirect('/publication/show/'.$id);
    }

    public function delete($id){
        $this->view = FALSE;
        $publication = $this->publication->delete($id);
        if(!$publication){
            $this->data['error'] = "error delete record";
        }else{
            redirect('/publication');
        }
    }
}
?>
