<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Team_model extends MY_Model
{
    public $table = 'team';
    public $primary_key = 'id';
    public $fillable = array('name','profile','link','photo');
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|required'),
            'profile' => array(
                'field'=>'profile',
                'label'=>'Profil',
                'rules'=>'trim|required')
        )
    );
}
?>
