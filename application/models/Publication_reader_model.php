<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Publication_reader_model extends MY_Model
{
    public $table = 'publication_reader';
    public $primary_key = 'id';
    public $fillable = array(
        'firstname',
        'lastname',
        'email',
        'phone',
        'position',
        'company',
        'address',
        'publication');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_one = array(
            'publication' => array('Publication_model','id','publication')
        );
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'firstname' => array(
                'field'=>'firstname',
                'label'=>'Nama Depan',
                'rules'=>'trim|required'),
            'email' => array(
                'field'=>'email',
                'label'=>'Email',
                'rules'=>'valid_email|required'),
            'phone' => array(
                'field'=>'phone',
                'label'=>'Phone Number',
                'rules'=>'trim|required'),
            'position' => array(
                'field'=>'position',
                'label'=>'Jabatan / Posisi',
                'rules'=>'trim|required'),
            'company' => array(
                'field'=>'company',
                'label'=>'Nama Perusahaan',
                'rules'=>'trim|required'),
            'address' => array(
                'field'=>'address',
                'label'=>'Alamat Perusahaan',
                'rules'=>'trim|required')
        )
    );
}
?>
