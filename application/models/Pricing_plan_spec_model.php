<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing_plan_spec_model extends MY_Model
{
    public $table = 'pricing_plan_spec';
    public $primary_key = 'id';
    public $fillable = array('spec','pricing_spec','pricing_plan');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_one = array(
            'pricing_plan' => array('Pricing_plan_model','id','pricing_plan'),
            'pricing_spec' => array('Pricing_spec_model','id','pricing_spec')
        );
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'spec' => array(
                'field'=>'spec',
                'label'=>'Spesifikasi',
                'rules'=>'trim|required')
        )
    );
}
?>
