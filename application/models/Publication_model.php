<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Publication_model extends MY_Model
{
    public $table = 'publication';
    public $primary_key = 'id';
    public $fillable = array(
        'title',
        'author',
        'cover',
        'content',
        'file',
        'published_at');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_many = array(
            'readers' => array('Publication_reader_model','publication','id')
        );
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'author' => array(
                'field'=>'author',
                'label'=>'Pengarang',
                'rules'=>'trim|required'),
            'title' => array(
                'field'=>'title',
                'label'=>'Judul',
                'rules'=>'trim|required'),
            'content' => array(
                'field'=>'content',
                'label'=>'Isi / Konten',
                'rules'=>'trim|required'),
            'published_at' => array(
                'field'=>'published_at',
                'label'=>'tanggal publikasi',
                'rules'=>'required')
        )
    );
}
?>
