<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends MY_Model
{
    public $table = 'contact';
    public $primary_key = 'id';
    public $fillable = array(
        'address',
        'city',
        'zipcode',
        'phone',
        'fax',
        'state',
        'twitter',
        'facebook',
        'linkedin',
        'lat',
        'long'
    );
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
    }

    public $rules = array();
}
?>
