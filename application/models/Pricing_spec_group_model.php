<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing_spec_group_model extends MY_Model
{
    public $table = 'pricing_spec_group';
    public $primary_key = 'id';
    public $fillable = array('name');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_many = array(
            'specs' => array('Pricing_spec_model','group','id')
        );
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|required')
        )
    );
}
?>
