<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends MY_Model
{
    public $table = 'page';
    public $primary_key = 'id';
    public $fillable = array(
        'title',
        'subtitle',
        'slug',
        'content');
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'subtitle' => array(
                'field'=>'subtitle',
                'label'=>'SubJudul',
                'rules'=>'trim|required'),
            'title' => array(
                'field'=>'title',
                'label'=>'Judul',
                'rules'=>'trim|required'),
            'content' => array(
                'field'=>'content',
                'label'=>'Isi / Konten',
                'rules'=>'trim|required'),
            'slug' => array(
                'field'=>'slug',
                'label'=>'Url Slug',
                'rules'=>'trim|required')
        )
    );
}
?>
