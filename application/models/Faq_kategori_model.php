<?php
/**
 * Created by PhpStorm.
 * User: muhammadzulfaf
 * Date: 7/16/16
 * Time: 21:50
 */

class Faq_kategori_model extends  MY_Model{
    public $table = "faq_category";
    public $primary_key = "id_category";


    public function listAllCategory(){
        return $this->db->select("*")
            ->get($this->table)->result();
    }

    public function listCategoryById($id){
        return $this->db->select("*")
            ->where("id_category",$id)
            ->get($this->table)->result();
    }

    public function addNewFaqCategory($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function getById($id){
        return $this->db->select("*")
            ->where("id_category",$id)
            ->get($this->table)->result();
    }

    public function updateFaqCategory($data,$id){
        $this->db->where('id_category', $id);
        return $this->db->update($this->table, $data);
    }

    public function delete($id=null){
        $this->db->where('id_category', $id);
        return $this->db->delete($this->table);
    }
}