<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trial_request_model extends MY_Model
{
    public $table = 'trial_request';
    public $primary_key = 'id';
    public $fillable = array(
        'firstname',
        'lastname',
        'email',
        'phone',
        'position',
        'company',
        'address',
        'notes');
    public $protected = array('id');

    public $rules = array(
        'insert' => array(
            'firstname' => array(
                'field'=>'firstname',
                'label'=>'Nama Depan',
                'rules'=>'required|min_length[5]|max_length[15]'),
            'email' => array(
                'field'=>'email',
                'label'=>'Email',
                'rules'=>'trim|required|valid_email|is_unique[trial_request.email_id]'),
            'phone' => array(
                'field'=>'phone',
                'label'=>'Phone Number',
                'rules'=>'trim|required'),
            'position' => array(
                'field'=>'position',
                'label'=>'Jabatan / Posisi',
                'rules'=>'trim|required'),
            'company' => array(
                'field'=>'company',
                'label'=>'Nama Perusahaan',
                'rules'=>'trim|required'),
            'address' => array(
                'field'=>'address',
                'label'=>'Alamat Perusahaan',
                'rules'=>'trim|required')
        )
    );
}
?>
