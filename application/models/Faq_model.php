<?php
/**
 * Created by PhpStorm.
 * User: muhammadzulfaf
 * Date: 7/16/16
 * Time: 21:47
 */

class Faq_model extends CI_Model{
    public $table = "faq";
    public $primary_key = "id";

    public function listAllFaq(){
        return $this->db->select("*")
            ->get($this->table)->result();
    }

    public function getById($id){
        return $this->db->select("*")->where("id",$id)
            ->join('faq_category','faq.id_category = faq_category.id_category','left')
            ->get($this->table)->result();
    }

    public function listPerCategory($id){
        return $this->db->select("*")
            ->where("id_category",$id)
            ->get($this->table)->result();
    }

    public function addNewFaq($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function updateFaq($data,$id){
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    public function updateCategory($id,$data){
        $this->db->where('id_category', $id);
        $this->db->update($this->table, $data);
    }

    public function delete($id=null){
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
}