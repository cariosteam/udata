<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing_plan_model extends MY_Model
{
    public $table = 'pricing_plan';
    public $primary_key = 'id';
    public $fillable = array('name','color');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_many = array(
            'specs' => array('Pricing_plan_spec_model','pricing_plan','id')
        );
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|required'),
            'color' => array(
                'field'=>'color',
                'label'=>'Warna',
                'rules'=>'trim|required')
        )
    );
}
?>
