<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Why_us_model extends MY_Model
{
    public $table = 'why_us';
    public $primary_key = 'id';
    public $fillable = array('title','caption','image', 'content');
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'title' => array(
                'field'=>'title',
                'label'=>'Judul',
                'rules'=>'trim|required'),
            'caption' => array(
                'field'=>'caption',
                'label'=>'Caption',
                'rules'=>'trim|required'),
            'content' => array(
                'field'=>'content',
                'label'=>'Content',
                'rules'=>'trim|required')
        )
    );
}
?>
