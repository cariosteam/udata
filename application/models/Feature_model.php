<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feature_model extends MY_Model
{
    public $table = 'features';
    public $primary_key = 'id';
    public $fillable = array('title','name','content','illustration');
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|required'),
            'title' => array(
                'field'=>'title',
                'label'=>'Judul',
                'rules'=>'trim|required'),
            'content' => array(
                'field'=>'content',
                'label'=>'Isi / Konten',
                'rules'=>'trim|required')
        )
    );
}
?>
