<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing_spec_model extends MY_Model
{
    public $table = 'pricing_spec';
    public $primary_key = 'id';
    public $fillable = array('name','title','group');
    public $protected = array('id');

    public function __construct()
    {
        $this->has_many = array(
            'specs' => array('Pricing_plan_spec_model','pricing_spec','id')
        );

        $this->has_one = array(
            'group' => array('Pricing_spec_group_model','id','group')
        );

        parent::__construct();
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|required'),
            'title' => array(
                'field'=>'title',
                'label'=>'Judul',
                'rules'=>'trim|required')
        )
    );
}
?>
