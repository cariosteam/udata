<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends MY_Model
{
    public $table = 'admin';
    public $primary_key = 'id';
    public $fillable = array('name','password');
    public $protected = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public $rules = array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'Nama',
                'rules'=>'trim|alpha_numeric|required'),
            'password' => array(
                'field'=>'password',
                'label'=>'Password',
                'rules'=>'trim|required')
        )
    );

    public $before_create = array( 'hash_password' );
    public $before_update = array( 'hash_password' );

    protected function hash_password($data){
        if(!empty($data['password'])){
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        }
        return $data;
    }

    public function auth(){
        $name = $this->input->post('name');
        $password = $this->input->post('password');
        $remember = $this->input->post('remember');

        // get admin data
        $query = $this->db->get_where($this->table, array(
            'name'=>$name
        ), 1);
        $admin = $query->row();

        if(!$admin)
            return FALSE;

        // password validation
        if(!password_verify($password, $admin->password))
            return FALSE;

        // save session
        unset($admin->password);
        $this->session->set_userdata('admin', $admin);
        if(!$remember){
            // set session for an hour
            $this->session->mark_as_temp('admin', 3600);
        }

        return $admin;
    }

    public function isAuthenticated(){
        return $this->session->has_userdata('admin');
    }

    public function logout(){
        return $this->session->unset_userdata('admin');
    }
}
?>
