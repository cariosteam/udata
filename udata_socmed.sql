-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2016 at 04:17 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `udata_socmed`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `password` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'mcmbddds', '$2y$10$7IIPlA7Z30uz6Pj6buOU.u8JbSqCCwh4zlTQk4pxYaxjJy.2l9Pya', '2016-03-01 15:13:23', '2016-03-19 04:06:09'),
(2, 'usocmed', 'admin123', '2016-07-05 21:55:00', '2016-07-05 21:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address` text,
  `city` varchar(50) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `linkedin` varchar(50) DEFAULT NULL,
  `lat` varchar(200) NOT NULL,
  `long` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address`, `city`, `zipcode`, `phone`, `fax`, `state`, `twitter`, `facebook`, `linkedin`, `lat`, `long`, `created_at`, `updated_at`) VALUES
(1, 'Menara Multimedia Lt. 17 Jalan Kebon Sirih No.10 - 12', 'Jakarta Pusat', '40152', '(62-22) 201 450', '(62-22) 201 442', 'Daerah Khusus Ibukota Jakarta', 'telkomindonesia', '', '', '-6.1834890', '106.8286925', '2016-03-01 20:55:33', '2016-03-29 08:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `content` text,
  `illustration` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `title`, `name`, `content`, `illustration`, `created_at`, `updated_at`) VALUES
(1, 'When Wordcloud Is Outdated', 'Interactive Multilevel Pie Chart', '<p><strong>MORE INSIGHTS IN MORE EFFECTIVE MANNER</strong></p>\r\n<p>Thats what our innovative multilevel pie chart could offer to you. We diagnosed that the outdated wordcloud is incompetence to deliver deeper insights and unflexible to shows correlation between related keywords. Our interactive multilevel pie chart could accomodate that weaknesses and bring contextual insights.</p>\r\n<p>&nbsp;</p>', 'feature_57048ca23f4cc.jpg', '2016-03-01 18:37:26', '2016-06-27 04:25:48'),
(2, 'NLP in Bahasa Indonesia For Local Content', 'Localized Sentiment Analysis', '<p>TAME THE COMPLEX LOCAL LANGUAGE</p>\r\n<p>This is the gateway to access incredible insight into what 237 million Indonesian consumers are thinking, and what trends and potentially influencing their behaviours and choices.&nbsp;</p>\r\n<p>By the year of 2017, we will include machine learning to skyrocket the accuracy of our NLP intelligence.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'feature_57048def444c5.jpg', '2016-03-02 00:23:28', '2016-06-27 04:27:47'),
(4, 'Provide your member with customizable access', 'Team Management', '<p><span style="font-family: Helvetica, sans-serif;">ADD. EDIT. SEE.</span></p>\r\n<p><span style="font-family: Helvetica, sans-serif;">We let you control your social media analytics team in a way that you could maximize efficiency in generating insights from our service. The number of team members you can add and manage is up to 10 members depend on your subscriptions level.&nbsp;</span></p>\r\n<p style="text-align: start; mso-outline-level: 5;"><span style="font-family: Helvetica, sans-serif;">In addition, you can also control each of your member into three different access levels. An admin can create new topic and add someone else. In lower access member there are senior and junior members. A senior can manage or edit topics, meanwhile a junior can only see and generate results based on determined topics.</span></p>', 'feature_57048f77eb97f.jpg', '2016-03-02 00:27:49', '2016-05-30 15:05:14'),
(5, 'Analyze further who''s behind the noise', 'Influencers Mapping', '<p>MAKE IT CONTEXTUAL</p>\r\n<p>A conversation would be more meaningful if we know who was make the noise in the first place. We deliver you unrivaled feature to analyze the influencers in every generated conversation in the social media. Hence, you would be able to clear up the mist and draw clearer insights.</p>\r\n<p>We created mapping system to determine the influencers role to the given topics. we make profiling about it, process it further into demographics information with related keywords. in the end of the day you would be able to identify what are the stance of every influencers regarding to related topics.</p>\r\n<p>&nbsp;</p>', 'feature_57048ff89f7a5.jpg', '2016-03-16 04:42:58', '2016-05-30 15:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `help_slider`
--

CREATE TABLE `help_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `caption` varchar(200) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help_slider`
--

INSERT INTO `help_slider` (`id`, `title`, `caption`, `image`, `created_at`, `updated_at`) VALUES
(5, 'Identify who''s involved', 'Listening is not enough! We analyze who are the influencers behind', 'howhelp_570480af5f804.jpg', '2016-03-01 16:35:27', '2016-05-30 10:10:08'),
(6, 'Capture all the data from various sources', 'Twitter. Facebook. News sites. Blog sites. We covered it for you.', 'howhelp_56d6227e5560e.jpg', '2016-03-02 00:15:11', '2016-05-30 10:18:01'),
(7, 'Simplicity at its FINEST. We give you no hassle', 'Easily sorts and filters what you want to see. Our dashboard will redefine what monitoring is. ', 'howhelp_57047973ed0a6.jpg', '2016-03-02 00:15:40', '2016-05-30 14:15:07'),
(8, 'Drives company decision', 'Transforming unstructured conversation into meaningful insights', 'howhelp_56d622b9207d1.jpg', '2016-03-02 00:16:10', '2016-05-30 10:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `slug`, `subtitle`, `content`, `created_at`, `updated_at`) VALUES
(1, 'term of use', 'term-of-use', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', '<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum, dolor vitae fringilla efficitur, elit justo congue quam, ac finibus tellus risus vel tellus. Suspendisse at nunc rhoncus, lacinia leo ut, sollicitudin lorem. Aliquam interdum lorem erat, vel aliquam tellus semper in. Etiam nisl sem, suscipit vel blandit nec, gravida id risus. Nunc mollis pellentesque metus, ac venenatis lorem egestas et. Vivamus mollis nisl ac nisl posuere lacinia. Cras tellus arcu, sagittis in molestie in, porta quis tortor.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Maecenas vitae varius lorem, ullamcorper viverra magna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam commodo eget lectus in laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras vitae ornare lectus, a egestas nibh. Curabitur varius ultricies tellus ac scelerisque. Nam sit amet ante quis diam lacinia tempor vitae a lacus. Duis tempor placerat risus, ut semper neque tempus eu. Donec in tortor placerat, aliquet nisi eget, laoreet quam. Quisque scelerisque posuere massa, nec pulvinar velit tristique vel. Sed placerat egestas leo, non vehicula risus facilisis ut. Phasellus quis dignissim nisl, nec &nbsp;sdsds euismod tortor. Quisque pharetra turpis tellus, et porttitor quam posuere sed.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Sed at aliquam est. Maecenas dui est, blandit in tincidunt sit amet, molestie ut ipsum. Nunc vitae ex finibus, vehicula orci id, lobortis nunc. Etiam ac enim consequat, maximus nunc ac, venenatis risus. In hac habitasse plat ea dictumst. Pellentesque egestas congue porttitor. Proin ut arcu viverra, varius odio quis, fermentum ipsum. Donec iaculis, metus ac maximus lacinia, sapien neque vulputate sapien, at aliquet lorem felis in mauris. Nunc vulputate nisi tincidunt pellentesque pulvinar. Curabitur lacinia elit eget lacus molestie, id scelerisque tellus mollis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Etiam vitae justo ac tortor posuere tristique ut ut ante. Praesent vel ligula quis leo congue consequat ut nec magna. Mauris ut mauris vulputate, porttitor neque sit amet, porta sapien. Ut sagittis eu nisl non egestas. Cras vel ligula at neque ultrices venenatis vulputate vitae nisl. Phasellus non arcu felis. Integer congue eros vitae hendrerit mattis. Phasellus ullamcorper sem laoreet orci dictum accumsan.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Phasellus varius luctus erat, ut dapibus risus accumsan vel. Aenean venenatis dui enim, sed pharetra mi maximus a. Mauris rhoncus velit nec porta malesuada. Maecenas tincidunt sodales magna, et tincidunt diam vehicula quis. Quisque luctus mauris vitae lectus interdum ultrices. Fusce ornare eros ipsum, sit amet commodo justo dictum a. Fusce hendrerit volutpat eros, a lobortis lorem vehicula a. Duis elementum magna sit amet dui consectetur tincidunt. Morbi imperdiet arcu vel purus maximus lobortis. Nulla imperdiet felis id libero bibendum, sed cursus magna ornare. Phasellus eu neque nec orci blandit mollis et at neque. Sed dictum libero et ante tempor pellentesque vitae sit amet augue. In auctor purus vel nibh cursus euismod ut vitae mi. Donec congue, dui eu euismod iaculis, mi neque imperdiet massa, quis lobortis risus purus vel risus.</p>', '2016-03-01 21:57:27', '2016-03-13 22:35:30'),
(2, 'data policy', 'data-policy', 'data policy', '<h3 style="margin: 0px 0px 14px; padding: 0px; font-size: 11px; font-family: Arial, Helvetica, sans;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>\r\n<h3 style="margin: 0px 0px 14px; padding: 0px; font-size: 11px; font-family: Arial, Helvetica, sans;">1914 translation by H. Rackham</h3>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>\r\n<h3 style="margin: 0px 0px 14px; padding: 0px; font-size: 11px; font-family: Arial, Helvetica, sans;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>\r\n<h3 style="margin: 0px 0px 14px; padding: 0px; font-size: 11px; font-family: Arial, Helvetica, sans;">1914 translation by H. Rackham</h3>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.</p>', '2016-03-13 22:37:40', NULL),
(3, 'faq', 'faq', 'faq', '<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Praesent sed libero vitae eros dignissim ultrices id nec quam. Nunc ornare rhoncus urna vitae porttitor. Ut viverra blandit neque non tempus. Nulla eu felis gravida, pellentesque arcu a, convallis quam. Mauris accumsan nunc quis dui pellentesque semper. Phasellus malesuada consectetur nibh, eget sagittis mi placerat et. Vestibulum mattis ante vel fermentum malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis, massa a porttitor dapibus, ex sapien volutpat leo, viverra ultrices ligula metus eu nunc. Curabitur consequat elit a suscipit scelerisque. Proin maximus sem ex. Morbi vel nisl et velit pretium ultricies quis vel leo. Mauris sed risus vitae est ullamcorper imperdiet.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">In ullamcorper venenatis felis. Nam scelerisque aliquam lacinia. Sed elementum lacinia nisi. Nam aliquam quis leo eu pulvinar. Aliquam quis eleifend odio, in porttitor ex. Nam pulvinar purus sem, id blandit sem sagittis eu. In viverra, nisi et malesuada tempor, quam eros ullamcorper nisi, nec sagittis nunc tortor at turpis. Proin sit amet nisl faucibus orci placerat tempus vel id lorem.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Mauris suscipit orci lacus, tincidunt laoreet arcu blandit ac. Aliquam maximus nisl ut nunc tristique ullamcorper. Quisque eget nisl consectetur, facilisis ipsum non, tristique justo. Sed at finibus lacus. Quisque ultricies tellus at lorem congue pharetra. Nunc et metus diam. Etiam cursus elementum maximus. Mauris hendrerit enim elit, in lobortis est ultrices dictum.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Vivamus non tellus vel neque sagittis interdum. Curabitur nec tempor neque, vitae aliquam mi. Duis semper eros in ipsum consectetur, sed fermentum ligula dapibus. Suspendisse vitae pharetra sapien, sit amet blandit arcu. Nunc sit amet rhoncus elit. Praesent luctus urna et ligula facilisis, eget pretium sapien aliquam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris nec finibus tortor, nec interdum quam. Nullam venenatis nec elit in convallis. Pellentesque condimentum nulla neque, quis volutpat nulla feugiat id.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nam nunc mauris, efficitur ut dui aliquam, viverra rutrum velit. Duis sit amet leo cursus mauris rhoncus maximus. Vivamus laoreet purus sed justo vestibulum pulvinar. Donec sagittis urna ac orci ullamcorper posuere. Vivamus non lobortis arcu, in gravida nunc. Aliquam luctus rhoncus nunc, a imperdiet massa dictum id. Mauris eu velit pharetra, tincidunt orci in, fermentum ante. Sed sit amet nibh tristique, vulputate dui a, pharetra neque. Aliquam quis massa vitae velit ultricies mollis. Vivamus sodales diam vitae consectetur porta. Nullam dictum libero tellus, in feugiat tellus interdum et.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Sed ut neque et arcu euismod pharetra at vel libero. Etiam finibus diam id leo consequat hendrerit. Vivamus laoreet diam vitae ultrices mattis. Donec feugiat rutrum mi, sed viverra tortor faucibus eu. Etiam commodo sem vitae odio scelerisque laoreet. Donec tincidunt condimentum arcu, nec egestas quam commodo eu. Etiam elementum venenatis eros, eu consectetur odio ullamcorper ut. Suspendisse potenti. Aenean aliquet arcu at odio aliquam mollis. Nunc quis eleifend quam. Vivamus nec lacinia metus, at porta tortor. Curabitur vehicula ultricies lacinia. Curabitur dignissim eros quis mollis iaculis. Vestibulum risus velit, ultricies accumsan vulputate vitae, tristique faucibus magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum, nisl vel elementum tempor, ex tellus facilisis massa, ac consectetur ante eros egestas ligula.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Curabitur purus libero, pellentesque pulvinar nulla ut, lobortis semper metus. Morbi sollicitudin gravida est sit amet interdum. Nulla velit nulla, scelerisque vitae velit quis, aliquet facilisis nibh. Suspendisse a est diam. Aenean purus erat, consequat eu nibh quis, elementum elementum magna. Etiam convallis libero vitae quam interdum, eu rutrum mi aliquet. Suspendisse quis purus tempus, molestie orci hendrerit, dignissim massa. Ut aliquet orci sed pharetra condimentum. Duis eu varius tellus, sagittis tempus neque. Nulla pellentesque justo varius augue pretium mollis. Donec turpis risus, gravida sed nibh id, tempor sagittis nibh. Etiam blandit lacinia nunc, sit amet vulputate est hendrerit eget. Aenean eu suscipit nibh, et scelerisque tortor.</p>', '2016-03-13 22:39:46', NULL),
(4, 'about', 'about', 'about socmed udata', '<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Proin dignissim leo sodales erat consequat, vulputate tempor ipsum semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec tempus tortor sit amet accumsan commodo. Vestibulum aliquam orci eleifend magna rutrum, quis bibendum magna volutpat. Suspendisse eu elit hendrerit, egestas nisi ut, cursus felis.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Phasellus faucibus erat sit amet eros convallis, facilisis bibendum purus volutpat. Vestibulum vel lobortis libero. Vivamus aliquet quam sit amet erat tempus, sit amet aliquet ante maximus. Suspendisse euismod sapien sit amet massa ullamcorper, sollicitudin fringilla ligula facilisis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas nisi ante, vulputate vitae ipsum id, vestibulum blandit sapien. Vivamus commodo, lorem ut aliquet finibus, justo nisi semper odio, ac tristique elit magna dignissim sapien. Aliquam quis semper mi. Aliquam vel dui volutpat, feugiat velit id, viverra mi. Integer ut pretium dui. Quisque et lacus vel lacus cursus sodales. Nulla tempus libero vitae nibh dapibus cursus. Quisque vestibulum dictum mauris, non finibus diam maximus laoreet. Integer sit amet est justo. Integer eu dui eget nisi accumsan cursus.</p>', '2016-03-13 22:40:15', '2016-03-14 15:58:30');

-- --------------------------------------------------------

--
-- Table structure for table `pricing_plan`
--

CREATE TABLE `pricing_plan` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing_plan`
--

INSERT INTO `pricing_plan` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(10, 'SILVER', '#d4d4d4', '2016-03-13 20:48:30', '2016-05-30 16:50:21'),
(11, '<b>GOLD</b>', '#f5db51', '2016-03-13 20:56:46', '2016-05-30 16:49:19'),
(12, '<b>PLATINUM</b>', 'rgba(235,50,9,0.98)', '2016-03-13 20:57:31', '2016-05-30 16:49:40');

-- --------------------------------------------------------

--
-- Table structure for table `pricing_plan_spec`
--

CREATE TABLE `pricing_plan_spec` (
  `id` int(11) NOT NULL,
  `spec` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pricing_plan` int(11) NOT NULL,
  `pricing_spec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing_plan_spec`
--

INSERT INTO `pricing_plan_spec` (`id`, `spec`, `created_at`, `updated_at`, `pricing_plan`, `pricing_spec`) VALUES
(17, '3 Alarms', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 9),
(19, 'Monthly', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 11),
(20, 'Daily response', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 12),
(22, '15', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 14),
(23, '2', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 15),
(24, 'Monthly', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 16),
(25, '1', '2016-03-13 20:48:31', '2016-05-30 16:50:21', 10, 17),
(27, '7 Alarms', '2016-03-13 20:56:46', '2016-05-30 16:49:19', 11, 9),
(29, 'Weekly', '2016-03-13 20:56:46', '2016-05-30 16:49:19', 11, 11),
(30, '12 Hours-cycle Response', '2016-03-13 20:56:46', '2016-05-30 16:49:19', 11, 12),
(32, '30', '2016-03-13 20:56:47', '2016-05-30 16:49:19', 11, 14),
(33, '4', '2016-03-13 20:56:47', '2016-05-30 16:49:19', 11, 15),
(34, 'Weekly', '2016-03-13 20:56:47', '2016-05-30 16:49:19', 11, 16),
(35, '3', '2016-03-13 20:56:47', '2016-05-30 16:49:19', 11, 17),
(37, 'Unlimited alarms', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 9),
(39, 'Unlimited', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 11),
(40, '3 Hours-cycle response', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 12),
(42, '80', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 14),
(43, '10', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 15),
(44, 'Unlimited', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 16),
(45, '10', '2016-03-13 20:57:32', '2016-05-30 16:49:40', 12, 17),
(47, 'Rp 13M', '2016-03-29 08:55:58', '2016-05-30 16:50:21', 10, 19),
(48, 'Rp 156M ', '2016-03-29 08:55:59', '2016-05-30 16:50:21', 10, 20),
(50, 'Rp 25M', '2016-03-29 08:57:51', '2016-05-30 16:49:19', 11, 19),
(51, 'Rp 275M (Free 1 month)', '2016-03-29 08:57:52', '2016-05-30 16:49:19', 11, 20),
(52, 'Rp 85M', '2016-05-30 16:47:05', '2016-05-30 16:49:40', 12, 19),
(53, 'Rp 850M (Free 2 Months)', '2016-05-30 16:47:05', '2016-05-30 16:49:40', 12, 20);

-- --------------------------------------------------------

--
-- Table structure for table `pricing_spec`
--

CREATE TABLE `pricing_spec` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `group` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing_spec`
--

INSERT INTO `pricing_spec` (`id`, `name`, `title`, `group`, `created_at`, `updated_at`) VALUES
(9, 'trackers_alert', 'trackers alert', 1, '2016-03-13 20:32:57', NULL),
(11, 'customized_report', 'customized report', 1, '2016-03-13 20:34:16', NULL),
(12, 'customer_support', 'customer support', 1, '2016-03-13 20:34:28', NULL),
(14, 'keywords', 'keywords', 3, '2016-03-13 20:34:59', '2016-03-13 20:35:23'),
(15, 'trackers', 'trackers', 3, '2016-03-13 20:35:07', '2016-03-13 20:35:14'),
(16, 'reports_created', 'reports created', 3, '2016-03-13 20:35:37', NULL),
(17, 'team_members', 'team members', 3, '2016-03-13 20:35:49', NULL),
(19, 'harga', 'Monthly subscription', 4, '2016-03-22 09:03:33', NULL),
(20, 'Annual_subscription', 'Annual Subscription (Free 1 Month)', 4, '2016-03-22 09:25:27', '2016-03-29 09:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `pricing_spec_group`
--

CREATE TABLE `pricing_spec_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing_spec_group`
--

INSERT INTO `pricing_spec_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'features', '2016-03-13 11:11:50', '2016-03-13 11:21:09'),
(3, 'limitation', '2016-03-13 11:21:20', NULL),
(4, 'Investment', '2016-03-22 09:01:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `cover` varchar(45) DEFAULT NULL,
  `file` varchar(200) NOT NULL,
  `content` text,
  `published_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `title`, `author`, `cover`, `file`, `content`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'Neque porro quisquam est qui dolorem ipsum quia do', 'test', 'publication_56d6033749aa5.jpg', 'test.pdf', '<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Etiam purus nisl, ultrices nec libero ac, tristique placerat sem. Sed vel massa justo. Sed placerat consectetur est vel scelerisque. Sed in elementum mi, commodo aliquet augue. Integer imperdiet erat quam, nec fringilla est efficitur ut. Maecenas sit amet efficitur ipsum. Duis laoreet ex vel odio imperdiet tempus. Quisque euismod sollicitudin nulla, vel maximus ante aliquam ac. In in pharetra ante. Nulla viverra lorem enim, non vestibulum felis venenatis vitae. Nulla luctus faucibus metus, eget eleifend mauris lobortis et. Praesent facilisis ex turpis, eget pharetra sem luctus quis. Donec at metus purus. Sed non tincidunt velit. Proin orci risus, feugiat at dolor non, sagittis lobortis lectus. Curabitur tincidunt odio nec libero lobortis congue.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Suspendisse eu condimentum est, eget convallis orci. Vestibulum condimentum est at enim sagittis, nec facilisis magna finibus. Morbi porttitor, est eu egestas rhoncus, nibh quam imperdiet eros, et finibus ligula magna in lectus. Curabitur at metus tortor. Proin rhoncus mauris at felis dictum viverra. Integer nisi ante, condimentum vel vehicula vitae, cursus non ex. Donec ut blandit lorem, id feugiat tellus. Phasellus ut congue augue. Nunc eu nulla nec arcu vestibulum convallis.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nullam lacinia nulla purus, a posuere dolor tristique eleifend. Vivamus bibendum tellus nec quam semper rutrum. Aliquam non maximus sapien. Sed mattis sit amet quam finibus viverra. Nullam congue mauris libero, at placerat erat molestie ac. Sed ultrices, nunc euismod consectetur vehicula, felis est rutrum sem, at tempor eros enim ac ex. Donec orci augue, aliquam sit amet arcu et, fringilla interdum dolor. Pellentesque accumsan eros ex, a tempor mi hendrerit ut.</p>\r\n<p style="text-align: justify; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Sed vitae semper odio. Nulla facilisis lacus sed ipsum maximus, nec facilisis velit vehicula. Nullam bibendum eros neque, vel malesuada elit dapibus vitae. Curabitur quis semper massa. Quisque at enim non augue vestibulum accumsan. Aliquam in nunc sed turpis tempor ultrices eu vitae ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas nunc libero, tristique at nisi in, dapibus interdum tellus. Phasellus a euismod enim. Suspendisse potenti. Nam facilisis pretium convallis.</p>', NULL, '2016-03-01 22:01:43', '2016-03-17 08:20:19'),
(2, 'Suspendisse eu condimentum', ' Vivamus bibendum', 'publication_56e6dccfc0557.jpg', 'test.pdf', '<p><span style="font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;">Etiam purus nisl, ultrices nec libero ac, tristique placerat sem. Sed vel massa justo. Sed placerat consectetur est vel scelerisque. Sed in elementum mi, commodo aliquet augue. Integer imperdiet erat quam, nec fringilla est efficitur ut. Maecenas sit amet efficitur ipsum. Duis laoreet ex vel odio imperdiet tempus. Quisque euismod sollicitudin nulla, vel maximus ante aliquam ac. In in pharetra ante. Nulla viverra lorem enim, non vestibulum felis venenatis vitae. Nulla luctus faucibus metus, eget eleifend mauris lobortis et. Praesent facilisis ex turpis, eget pharetra sem luctus quis. Donec at metus purus. Sed non tincidunt velit. Proin orci risus, feugiat at dolor non, sagittis lobortis lectus. Curabitur tincidunt odio nec libero lobortis congue.</span></p>', NULL, '2016-03-14 16:46:24', NULL),
(3, 'Nam facilisis pretium convallis', 'Nullam bibendum', 'publication_56e6dcf5825c5.jpg', 'test.pdf', '<p><span style="font-family: Arial, Helvetica, sans; line-height: 14px; text-align: justify;">Sed vitae semper odio. Nulla facilisis lacus sed ipsum maximus, nec facilisis velit vehicula. Nullam bibendum eros neque, vel malesuada elit dapibus vitae. Curabitur quis semper massa. Quisque at enim non augue vestibulum accumsan. Aliquam in nunc sed turpis tempor ultrices eu vitae ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas nunc libero, tristique at nisi in, dapibus interdum tellus. Phasellus a euismod enim. Suspendisse potenti. Nam facilisis pretium convallis.</span></p>', NULL, '2016-03-14 16:47:02', NULL),
(5, 'test', 'test', 'publication_56e7094d859b8.jpg', 'publication_56e7094e5ecd0.jpg', '<p>test</p>', NULL, '2016-03-14 19:56:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publication_reader`
--

CREATE TABLE `publication_reader` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text,
  `publication` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication_reader`
--

INSERT INTO `publication_reader` (`id`, `firstname`, `lastname`, `email`, `phone`, `position`, `company`, `address`, `publication`, `created_at`, `updated_at`) VALUES
(1, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'jl.sulanjana', 3, '2016-03-14 18:54:29', NULL),
(2, 'hasan', 'muhammad', 'hasan@lussa.net', 'a', 'CTO', 'lussa technology', 'a', 2, '2016-03-14 19:23:17', NULL),
(3, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Jl. Sulanjana 17', 2, '2016-03-14 19:34:32', NULL),
(4, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Jl. Sulanjana 17', 2, '2016-03-14 19:36:45', NULL),
(5, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Jl. Sulanjana 17', 2, '2016-03-14 19:36:50', NULL),
(6, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Jl. Sulanjana 17', 2, '2016-03-14 19:36:54', NULL),
(7, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Jl. Sulanjana 17', 2, '2016-03-14 19:38:30', NULL),
(8, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 2, '2016-03-14 19:40:59', NULL),
(9, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 2, '2016-03-14 19:41:55', NULL),
(10, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 2, '2016-03-14 19:42:10', NULL),
(11, 'hasan', 'muhammad', 'hasan@lussa.net', 'a', 'CTO', 'lussa technology', 'ss', 2, '2016-03-14 19:42:24', NULL),
(12, 'hasan', 'muhammad', 'hasan@lussa.net', 'test', 'CTO', 'lussa technology', 'test', 5, '2016-03-14 19:56:47', NULL),
(13, 'hasan', 'muhammad', 'hasan@lussa.net', '085287423805', 'CTO', 'lussa technology', 'Bangbayang Residence Jl. bangbayang no 23 kav B2 , Kamar no 18', 2, '2016-03-16 04:37:17', NULL),
(14, 'Arwin', 'Rahman', 'Arwinrahman@yahoo.com', '54534534q', 'CRM Analyst - Multichannel Big Data', 'Telekomunikasi Indonesia', 'gfgfhh', 1, '2016-03-21 07:52:39', NULL),
(15, 'Arwin', 'Rahman', 'Arwinrahman@yahoo.com', '08423423', 'CRM Analyst - Multichannel Big Data', 'Telekomunikasi Indonesia', 'kjfsdufhsd', 2, '2016-03-21 08:05:19', NULL),
(16, 'Arwin', 'Rahman', 'Arwinrahman@yahoo.com', 'hgjg', 'CRM Analyst - Multichannel Big Data', 'Telekomunikasi Indonesia', 'tygy', 2, '2016-03-21 08:10:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `profile` text,
  `link` text NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `profile`, `link`, `photo`, `created_at`, `updated_at`) VALUES
(5, 'Edwin Purwandesi', '<b>Business Development</b><br><br>\r\nBesides the routine, very passionate about drone technology', 'https://id.linkedin.com/in/edwin-purwandesi-71a19623', 'team_570490f939716.jpg', '2016-04-06 06:30:49', '2016-05-30 10:26:43'),
(6, 'Agus Laksono', '<b>Head of Data Scientist</b><br><br>\r\nHas strong background from financial industries. Often makes an offer that people cant refuse.', 'https://www.linkedin.com/in/agus-laksono-a9824490', 'team_5704915f6934c.jpg', '2016-04-06 06:32:31', '2016-05-30 10:27:48'),
(7, 'Komang Aryasa', '<b>Deputy of Analytics</b><br><br>\r\nUndoubtedly has inspired a lots of employees about the shining future of big data tech. ', 'https://www.linkedin.com/in/komang-aryasa-b74b6b34', 'team_570491e743529.jpg', '2016-04-06 06:34:47', '2016-05-30 10:28:44');

-- --------------------------------------------------------

--
-- Table structure for table `trial_request`
--

CREATE TABLE `trial_request` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text,
  `notes` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trial_request`
--

INSERT INTO `trial_request` (`id`, `firstname`, `lastname`, `email`, `phone`, `position`, `company`, `address`, `notes`, `created_at`, `updated_at`) VALUES
(1, 'hasan', 'muhammad', 'mhmad_hasan@lussa.net', '085287423805', 'CTO', 'Lussa Teknologi', 'jl.sulanjana 17 Bandung', '-', '2016-03-02 03:56:21', '2016-03-02 03:56:21'),
(2, NULL, NULL, 'hasan@lussa.net', 'a', 'CTO', 'lussa technology', 'a', NULL, '2016-03-14 19:16:46', NULL),
(3, NULL, NULL, 'hasan@lussa.net', '123', 'CTO', 'lussa technology', 'a', NULL, '2016-03-14 19:19:29', NULL),
(4, NULL, NULL, 'a', 'a', 'a', 'a', 'a', NULL, '2016-03-14 19:21:51', NULL),
(5, NULL, NULL, 'a', 'a', 'a', 'a', 'a', NULL, '2016-03-14 19:22:08', NULL),
(6, NULL, NULL, 'abcd', 'abcd', 'abcd', 'abcd', 'abcd', NULL, '2016-03-23 10:16:00', NULL),
(7, NULL, NULL, 'Arwinrahman@yahoo.com', '+6281214876707', 'CRM Analyst - Multichannel Big Data', 'Telekomunikasi Indonesia', 'Jl. Gegerkalong hilir no.47', NULL, '2016-04-07 04:32:13', NULL),
(8, NULL, NULL, 'Arwinrahman@yahoo.com', '+6281214876707', 'CRM Analyst - Multichannel Big Data', 'Telekomunikasi Indonesia', 'Jl. Gegerkalong hilir no.47', NULL, '2016-04-07 04:39:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `why_us`
--

CREATE TABLE `why_us` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `caption` varchar(200) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `why_us`
--

INSERT INTO `why_us` (`id`, `title`, `caption`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Localized<br/>Sentiment Analysis', 'Our intelligent and everlearn Natural Language Processing (NLP) is designed to capture content written in Bahasa & English', 'Tes Isi Konten Modal Why Us 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium sollicitudin luctus. Duis at magna accumsan nunc rhoncus sodales. Mauris nec quam elit. Quisque sit amet ipsum ante. Praesent at placerat est.', 'whyus_57048a09daaea.jpg', '2016-03-01 23:57:10', '2016-04-06 06:04:02'),
(2, 'Contextual<br/>Analysis', 'We let you experience higher degree of freedom to analyze in order to extract relevant findings', 'Tes Isi Konten Modal Why Us 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium sollicitudin luctus. Duis at magna accumsan nunc rhoncus sodales. Mauris nec quam elit. Quisque sit amet ipsum ante. Praesent at placerat est.', 'whyus_570484375f553.jpg', '2016-03-01 23:57:43', '2016-04-06 05:42:58'),
(3, 'Anytime & Anywhere <br/>Ready for service', 'Monitor with ease. Our cloud-based solution & strong infrastructure made this possible.', 'Tes Isi Konten Modal Why Us 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium sollicitudin luctus. Duis at magna accumsan nunc rhoncus sodales. Mauris nec quam elit. Quisque sit amet ipsum ante. Praesent at placerat est.', 'whyus_5704886593d0e.jpg', '2016-03-01 23:58:09', '2016-04-06 05:54:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_slider`
--
ALTER TABLE `help_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing_plan`
--
ALTER TABLE `pricing_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing_plan_spec`
--
ALTER TABLE `pricing_plan_spec`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pricing_plan_spec_pricing_plan_idx` (`pricing_plan`),
  ADD KEY `fk_pricing_plan_spec_pricing_spec1_idx` (`pricing_spec`);

--
-- Indexes for table `pricing_spec`
--
ALTER TABLE `pricing_spec`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pricing_spec_pricing_spec_group1_idx` (`group`);

--
-- Indexes for table `pricing_spec_group`
--
ALTER TABLE `pricing_spec_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication_reader`
--
ALTER TABLE `publication_reader`
  ADD PRIMARY KEY (`id`,`publication`),
  ADD KEY `fk_publication_reader_publication1_idx` (`publication`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trial_request`
--
ALTER TABLE `trial_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `why_us`
--
ALTER TABLE `why_us`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `help_slider`
--
ALTER TABLE `help_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pricing_plan`
--
ALTER TABLE `pricing_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pricing_plan_spec`
--
ALTER TABLE `pricing_plan_spec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `pricing_spec`
--
ALTER TABLE `pricing_spec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pricing_spec_group`
--
ALTER TABLE `pricing_spec_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `publication_reader`
--
ALTER TABLE `publication_reader`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `trial_request`
--
ALTER TABLE `trial_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `why_us`
--
ALTER TABLE `why_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pricing_plan_spec`
--
ALTER TABLE `pricing_plan_spec`
  ADD CONSTRAINT `fk_pricing_plan_spec_pricing_plan` FOREIGN KEY (`pricing_plan`) REFERENCES `pricing_plan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pricing_plan_spec_pricing_spec1` FOREIGN KEY (`pricing_spec`) REFERENCES `pricing_spec` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pricing_spec`
--
ALTER TABLE `pricing_spec`
  ADD CONSTRAINT `fk_pricing_spec_pricing_spec_group1` FOREIGN KEY (`group`) REFERENCES `pricing_spec_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `publication_reader`
--
ALTER TABLE `publication_reader`
  ADD CONSTRAINT `fk_publication_reader_publication1` FOREIGN KEY (`publication`) REFERENCES `publication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
